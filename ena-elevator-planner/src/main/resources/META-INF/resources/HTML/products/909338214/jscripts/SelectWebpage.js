// This is not currently being used. It was moved to
// HTML: /concept/custom/jpocs/kone/ena_elevator_planner/selected_product_details.html


function linkOpen() {
    var input = $('#ENA_ELEVATOR_GD_TOP_SELECT').val();
  
    if (input.includes('MONOSPACE_300')) {
      popupRedirectToolbox('http://ecospace.hostctb.com/');
    }
    if (input.includes('MINISPACE')) {
      popupRedirectToolbox('http://www.parametrx.com/KONE/EcoSystemMR/templates/basicAndSpec.asp');
    }
  
  
  }
  
  
  function popupRedirectToolbox(url) {
    try {
      // Get product name
      let productName = $('#ENA_ELEVATOR_GD_TOP_SELECT :selected').text().trim();
  
      // Make the configure button open a link on a new tab
      const changeButton = function (warning) {
        warning.find('.okay-btn').on('click', function () {
          window.open(url, '_blank')
        });
      }
  
      // Popup build options
      let options = {
        okay_btn: true,
        okay_btn_txt: ResourceBundle.getString("KONE_ESCALATOR_BUTTON_continue"),
        title: ResourceBundle.getString("KONE_WARNING_TITLE_please_note"),
        custom: changeButton   // Modify button
      };
  
      // Set message based on tab
      if (topMenuMgr.currentTab == "PRODUCT") {
        options.message = ResourceBundle.getString("KONE_ELEVATOR_TEXT_redirect_product").replace(/{productName}/g, productName);
       
      } else if (topMenuMgr.currentTab == "APPLICATION") {
        options.message = ResourceBundle.getString("KONE_ELEVATOR_TEXT_redirect_application").replace(/{productName}/g, productName);
      }
  
      // Build and destroy popup
      return new KoneDialog(options);
  
    } catch (e) {
      console.error(e);
    }
  }
  