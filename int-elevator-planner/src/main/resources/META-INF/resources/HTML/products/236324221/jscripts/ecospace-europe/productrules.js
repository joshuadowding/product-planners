//EcoSpace EU
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    compRegFilter();
}


//EcoSpace minimum SH calculation
function minSHCalc() {
    //console.debug("minSHCalc()");
    //get interface values
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());

    if (DIM_CAR_BALUSTRADE_HK == 700) {
        DIM_CAR_HEIGHT_CH += 1300;
    } else {
        DIM_CAR_HEIGHT_CH += 1700;
    }

    //set interface input
    DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

    //logging
    /*
     console.info("compRegEN81_72=" + compRegEN81_72);
     console.info("VAL_RATED_SPEED=" + VAL_RATED_SPEED);
     console.info("DIM_CAR_HEIGHT_CH=" + DIM_CAR_HEIGHT_CH);
     console.info("DIM_HEADROOM_SH=" + DIM_HEADROOM_SH.val());
     console.info("DIM_CAR_BALUSTRADE_HK=" + DIM_CAR_BALUSTRADE_HK);
     */

    //get Country Headroom Extension value from attribute, then add to DIM_HEADROOM_SH
    /*
    var inputName = "KONE_COUNTRY_C";
    var inputValue = $('#' + inputName).val();
    var attributeName = "Headroom_Extension";
    var query = "?id=" + configurationId
            + "&iName=" + inputName
            + "&iVal=" + inputValue
            + "&aName=" + attributeName;
    $.getJSON("/spr/Configuration/interface/api/beta/attribute" + query, function (data) {
        //	parse data
        try {
            if (data.option[0]) {
                var attrValue = parseFloat(data.option[0].result[0].value[0]);//attribute value
                var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
                var headroom = parseFloat(DIM_HEADROOM_SH.val()) + attrValue;
                DIM_HEADROOM_SH.val(headroom);

                //logging

                 console.info("Attribute Value=" + attrValue);
                 console.info("DIM_HEADROOM_SH=" + DIM_HEADROOM_SH.val());

                setValues();//update values for tables
            }
        } catch (err) {
            dumpError(err);
        }
    });
    */
    // KONE_COUNTRY_C is an user input and coonnot be used in this purpose, rejsli 060818
    // Only in NL the extension is visible in SH, 2019-08-21 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();
    var Extension = 0

    if (Loc == 'NL' || Loc == 'NL192') { //Netherlands
        var Extension = 200;
    }
    /*  if (Loc == 'UK') { //United Kingdom
     var Extension = 110;
    }
      if (Loc == 'IE') { //Ireland
     var Extension = 292;
    }
       if (Loc == 'NI') { //Northern Ireleand, user group value to be found out
     var Extension = 303;
    }*/

    var headroom = parseFloat(DIM_HEADROOM_SH.val()) + Extension;
    DIM_HEADROOM_SH.val(headroom);

    //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);

}

function minPHCalc() { 
  console.log("minPHCalc doesn't do anything yet"); 
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    }

    else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//2019-10-03 C1-VC	Changed format to be location dependant and faster
function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "ECO_SPACE_EUR"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();
    // NL 12-12-2018 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();

    var regulations = [];

    // Country overrides
    switch (Loc) {
        case 'NL':  //Netherlands
            regulations['ECO_SPACE_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_73'];
            regulations['ECO_SPACE_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_73'];
            break;

        default:    // Defaults values if location has no match
            regulations['ECO_SPACE_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77'];
            regulations['ECO_SPACE_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77'];
            break;

    }

   /*
    if (Loc == 'NL') { //Netherlands
        regulations['ECO_SPACE_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_73'];
        regulations['ECO_SPACE_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_73'];
    } else {
        regulations['ECO_SPACE_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77'];
        regulations['ECO_SPACE_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77'];
    } */

    var getReg = regulations[product + " " + mainReg];

    applyCompRegFilter(getReg);
}


