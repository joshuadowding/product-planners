// MonoSpace 500 EUR
$(function () {
    initRules();
});

function initRules() {
    minSHCalc();
    minPHCalc();
    compRegFilter();
}

// MS500 minimum SH calculation
// KPT-1636 changes 2020-12-03 rejsli
function minSHCalc() {
    //get interface values
    var compRegEN81_72 = false;

    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());
    var DEFAULT_HEADROOM_HEIGHT = parseFloat($('[name=DEFAULT_HEADROOM_HEIGHT]').val());
    var HEADROOM_HEIGHT = parseFloat($('[name=HEADROOM_HEIGHT]').val());
    var DIM_HEADROOM_HEIGHT_MIN = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MIN]').val());
    var DIM_HEADROOM_HEIGHT_MAX = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MAX]').val());
    var TEMP_MIN_HEADROOM = parseFloat($('[name=TEMP_MIN_HEADROOM]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var TYP_LDO_FRAME_FRONT = $('[name=TYP_LDO_FRAME_FRONT]').val();
    var TYP_LOW_HEADROOM_OPTION = $('[name=TYP_LOW_HEADROOM_OPTION]').val();
    var NO_HEADROOM_OPTION = $('[name=NO_HEADROOM_OPTION]').val();
    var PH_SH_COMBINATION = parseFloat($('[name=PH_SH_COMBINATION]').val()); //KPT-1636
    var PIT_DEPTH = parseFloat($('[name=PIT_DEPTH]').val()); //KPT-1636

    NO_HEADROOM_OPTION = 0;

    //check if EN81-72 selected
    if ($('#TYP_ELEV_STANDARD_COMPLEMENT_EN81_72').is(":checked")) {
        compRegEN81_72 = true;
    }

    if (DIM_CAR_BALUSTRADE_HK == 700) {
        if (!compRegEN81_72) {
            if (VAL_RATED_SPEED <= 1.0) {
                DIM_CAR_HEIGHT_CH += 1300;
            } else {
                DIM_CAR_HEIGHT_CH += 1500;
            }
        } else {
            if (VAL_RATED_SPEED <= 1.0) {
                DIM_CAR_HEIGHT_CH += 1380;
            } else {
                DIM_CAR_HEIGHT_CH += 1580;
            }
        }
    } else {
        if (!compRegEN81_72) {
            if (VAL_RATED_SPEED <= 1.0) {
                DIM_CAR_HEIGHT_CH += 1700;
            } else {
                DIM_CAR_HEIGHT_CH += 1900;
            }
        } else {
            if (VAL_RATED_SPEED <= 1.0) {
                DIM_CAR_HEIGHT_CH += 1780;
            } else {
                DIM_CAR_HEIGHT_CH += 1980;
            }
        }
    }

    //set interface input
    DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

    // Only in NL the extension is visible in SH, 2019-08-21 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();
    var Extension = 0

    if (Loc == 'NL' || Loc == 'NL192') { //Netherlands
        var Extension = 200;
    }

    var headroom = parseFloat(DIM_HEADROOM_SH.val()) + Extension;
    DIM_HEADROOM_SH = headroom;

    document.ui["DIM_HEADROOM_SH"].value = DIM_HEADROOM_SH;

    //Low headroom
    //Default and max value to Headroom, 2019-11-28 rejsli
    //If HEADROOM_AND_PIT_SECTION = 1, visible

    if (HEADROOM_AND_PIT_SECTION == 1) {
        var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val())
        var Add = 0;
        var warning = 0;
        var Combined = 0;

        //If LOW or NO heardoom are possible
        //NO headroom needs KES202 and that is only available as frame and if other counties come only to CH and DE

        if (TYP_LOW_HEADROOM_OPTION != "0") {
            //	if (TYP_LOW_HEADROOM_OPTION == "NO") {
            //       TEMP_MIN_HEADROOM = 2500;
            //	}
            //Narrow Frame is KES600 needs height operator
            if (TYP_LDO_FRAME_FRONT == "NARROWFRAME" && TEMP_MIN_HEADROOM < 2750) {
                TEMP_MIN_HEADROOM = 2750;
            }

            if (DIM_CAR_HEIGHT_CH == 2200) {
                var Add = 100;
            }

            var Minimum = TEMP_MIN_HEADROOM + Add;

        } else {
            TEMP_MIN_HEADROOM = headroom;
        }

        DEFAULT_HEADROOM_HEIGHT = DIM_HEADROOM_SH;

        //KPT-1636, combined PH + SH checked
        Combined = HEADROOM_HEIGHT + PIT_DEPTH;
        if (PH_SH_COMBINATION > Combined) {
            Minimum = PH_SH_COMBINATION - PIT_DEPTH;
            HEADROOM_HEIGHT = PH_SH_COMBINATION - PIT_DEPTH;
        }

        DIM_HEADROOM_HEIGHT_MAX = 4200;


        // Travel restriction	
        if (DIM_TRAVEL_HEIGHT_H >= 30000 && (WGT_RATED_LOAD_Q == 630 || WGT_RATED_LOAD_Q >= 1000)) {
            TYP_REGULATION_EN8121 = 0;
        }
        if (DIM_TRAVEL_HEIGHT_H >= 20000 && WGT_RATED_LOAD_Q == 680) {
            TYP_REGULATION_EN8121 = 0;
        }

        if (TYP_REGULATION_EN8121 == 1) {
            DIM_HEADROOM_HEIGHT_MIN = Minimum;
            warning = DEFAULT_HEADROOM_HEIGHT;
        } else {
            DIM_HEADROOM_HEIGHT_MIN = headroom;
            warning = headroom;
        }

        if (isNaN(HEADROOM_HEIGHT) || HEADROOM_HEIGHT < DIM_HEADROOM_HEIGHT_MIN || HEADROOM_HEIGHT > DIM_HEADROOM_HEIGHT_MAX) {
            HEADROOM_HEIGHT = DIM_HEADROOM_SH;
        } else {
            HEADROOM_HEIGHT = HEADROOM_HEIGHT;
        }

        if (HEADROOM_HEIGHT < 2600) {
            NO_HEADROOM_OPTION = 1;
        } else {
            NO_HEADROOM_OPTION = 0;
        }

        //set interface inputs
        document.ui["DIM_HEADROOM_HEIGHT_MIN"].value = DIM_HEADROOM_HEIGHT_MIN;
        document.ui["DIM_HEADROOM_HEIGHT_MAX"].value = DIM_HEADROOM_HEIGHT_MAX;
        document.ui["DEFAULT_HEADROOM_HEIGHT"].value = DEFAULT_HEADROOM_HEIGHT;
        document.ui["HEADROOM_HEIGHT"].value = HEADROOM_HEIGHT;
        document.ui["DIM_HEADROOM_SH"].value = HEADROOM_HEIGHT;
        document.ui["HEADROOM_HEIGHT_low"].value = DIM_HEADROOM_HEIGHT_MIN;
        document.ui["HEADROOM_HEIGHT_high"].value = DIM_HEADROOM_HEIGHT_MAX;
        document.ui["NO_HEADROOM_OPTION"].value = NO_HEADROOM_OPTION;

        //document.ui["HEADROOM_HEIGHT_warninglow"].value = warning;
        //document.ui["HEADROOM_HEIGHT_warninghigh"].value = standard.high;

        // We don't want to do the above or warning popup will begin showing up.
        // This was a custom solution. Set the warninglow/warninghigh in this
        // fashion. C1 (VC) 2019-12-23.
        document.getElementById('HEADROOM_HEIGHT').setAttribute('warninglow', warning);
        //document.getElementById('HEADROOM_HEIGHT').setAttribute('warninghigh', warning);
        updateDataTableInput(document.ui.HEADROOM_HEIGHT);
    }


    //this always needs to be run last so the DIM_HEADROOM_SH changes can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);

    // C1 - VC 2020-01-16:  this input doesn't always exist so prevents breaking the entire page.
    if (document.ui.NO_HEADROOM_OPTION)
        forceChangeEvent(document.ui.NO_HEADROOM_OPTION);
}

//MS500 minimum PH calculation
//KPT-1636 changes 3030-12-03 rejsli
function minPHCalc() {
    var DIM_PIT_HEIGHT_PH = parseFloat($('[name=DIM_PIT_HEIGHT_PH]').val());
    var DEFAULT_PIT_DEPTH = parseFloat($('[name=DEFAULT_PIT_DEPTH]').val());
    var PIT_DEPTH = parseFloat($('[name=PIT_DEPTH]').val());
    var DIM_PIT_DEPTH_MIN = parseFloat($('[name=PIT_DEPTH_MIN]').val());
    var DIM_PIT_DEPTH_MAX = parseFloat($('[name=PIT_DEPTH_MAX]').val());
    //var TEMP_MIN_PIT = parseFloat($('[name=TEMP_MIN_PIT]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var PH_SH_COMBINATION = parseFloat($('[name=PH_SH_COMBINATION]').val()); //KPT-1636 
    var HEADROOM_HEIGHT = parseFloat($('[name=HEADROOM_HEIGHT]').val()); //KPT-1636 
    var TEMP_MIN_PIT = 0; //KPT-1636 
    var Warning = 0;
    var Normal = 0;
    var Combined = 0;

    if (HEADROOM_AND_PIT_SECTION == 1) {

        //Normal pit depth EN81-20
        if (VAL_RATED_SPEED == 1.0) {
            if (WGT_RATED_LOAD_Q == 320) {
                Normal = 1250;
            } else {
                Normal = 1050;
            }
        } else {
            if (WGT_RATED_LOAD_Q == 400 || WGT_RATED_LOAD_Q == 450 || WGT_RATED_LOAD_Q == 480) {
                Normal = 1390;
            } else {
                Normal = 1200;
            }
        }

        DIM_PIT_DEPTH_MAX = 1550;
        TEMP_MIN_PIT = 650; //KPT-1636 always 650

        // Travel restrictions	
        if (DIM_TRAVEL_HEIGHT_H >= 30000 && (WGT_RATED_LOAD_Q == 630 || WGT_RATED_LOAD_Q >= 1000)) {
            TYP_REGULATION_EN8121 = 0;
        }

        if (DIM_TRAVEL_HEIGHT_H >= 20000 && WGT_RATED_LOAD_Q == 680) {
            TYP_REGULATION_EN8121 = 0;
        }

        //KPT-1636, combined PH + SH checked
        Combined = HEADROOM_HEIGHT + 650;
        if (PH_SH_COMBINATION > Combined) {
            TEMP_MIN_PIT = PH_SH_COMBINATION - HEADROOM_HEIGHT;
            if (PIT_DEPTH < TEMP_MIN_PIT) {
                PIT_DEPTH = TEMP_MIN_PIT;
            }
        }

        DEFAULT_PIT_DEPTH = Normal;

        if (TYP_REGULATION_EN8121 == 1) {
            DIM_PIT_DEPTH_MIN = TEMP_MIN_PIT;
            if (WGT_RATED_LOAD_Q == 320) {
                Warning = 1250;
            } else {
                Warning = 1050;
            }
        } else {
            DIM_PIT_DEPTH_MIN = Normal;
            Warning = Normal;
        }


        if (isNaN(PIT_DEPTH) || PIT_DEPTH < DIM_PIT_DEPTH_MIN || PIT_DEPTH > DIM_PIT_DEPTH_MAX) {
            PIT_DEPTH = Normal;
        } else {
            PIT_DEPTH = PIT_DEPTH;
        }

        DIM_PIT_HEIGHT_PH = PIT_DEPTH;

        //set interface inputs
        document.ui["DIM_PIT_DEPTH_MIN"].value = DIM_PIT_DEPTH_MIN;
        document.ui["DIM_PIT_DEPTH_MAX"].value = DIM_PIT_DEPTH_MAX;
        document.ui["DEFAULT_PIT_DEPTH"].value = DEFAULT_PIT_DEPTH;
        document.ui["PIT_DEPTH"].value = PIT_DEPTH;
        document.ui["DIM_PIT_HEIGHT_PH"].value = DIM_PIT_HEIGHT_PH;
        //set range
        document.ui["PIT_DEPTH_low"].value = DIM_PIT_DEPTH_MIN;
        document.ui["PIT_DEPTH_high"].value = DIM_PIT_DEPTH_MAX;

        //document.ui["PIT_DEPTH_warninglow"].value = Warning;
        //document.ui["PIT_DEPTH_warninghigh"].value = standard.high;

        // We don't want to do the above or warning popup will begin showing up.
        // This was a custom solution. Set the warninglow/warninghigh in this
        // fashion. C1 (VC) 2019-12-23.
        document.getElementById('PIT_DEPTH').setAttribute('warninglow', Warning);
        //document.getElementById('PIT_DEPTH').setAttribute('warninghigh', Warning);
        updateDataTableInput(document.ui.PIT_DEPTH);

        //this always needs to be run last so the changes can take effect
        forceChangeEvent(document.ui.DIM_PIT_HEIGHT_PH);
    }
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    } else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//2018-10-23 rejsli release 18.1 GOST_33653 added
//2019-02-20 rejsli release 18.2 GOST_33984.1-2016 added
//2019-10-01 rejsli KPT-383 LAW 13
//2019-10-03 C1-VC	Changed format to be location dependant and have faster access.
// ---------------------------
//2020-05-15 C1-VC	Disabled to test new regulations method.
// 				Rename to 'compRegFilter' to revert to this method.
function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "MONOSPACE_500_EUR"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();

    var regulations = [];

    // Country overrides
    switch (Loc) {
        case 'BA': // Bosnia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'BG': // Bulgaria
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'CY': // Cyprus
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'EE': // Estonia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'ES': // Spain
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'GR': // Greece
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'HR': // Croatia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'IE': // Ireland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'IS': // Iceland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'IT': // Italy;
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77', 'LAW_13'];
            break;

        case 'LT': // Lithuania
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'LV': // Latvia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'ME': // Montenegro
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'PL': // Poland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'PT': // Portugal
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'RO': // Romania
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'SI': // Slovenia
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'TR': // Turkey
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'UK': // United Kingdom
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        default: // Defaults values if location has no match
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['GOST_53780'] = ['GOST_52382', 'GOST_33652', 'GOST_33653'];
            regulations['GOST_33984.1-2016'] = ['GOST_52382', 'GOST_33652', 'GOST_33653'];
            regulations['Pubel_2012'] = ['None'];
            break;
    }

    var getReg = regulations[mainReg];
    applyCompRegFilter(getReg);
}
