// N MiniSpace (Asia)
$(function () {
  rulesInit();
});

function rulesInit() {
  minSHCalc();
  compRegFilter();
  initRegulations();
}

function minPHCalc() {
  // Doesn't do anything. Prevents breaking filters as this function was attached
  // directly to inputs.
  return;
}

//NMiniSpace minimum SH calculation acc to DL1-02.08.001 M.2
// KET = Taiwan not in code yet -> Elevator planner is not going to be implemented
// no need for separate rules
// GOST_33984.1-2016 added, rejsli 100419
function minSHCalc() {
  //console.debug("minSHCalc()");
  //get interface values
  var VAL_RATED_SPEED = parseFloat($("[name=VAL_RATED_SPEED_MR]").val());
  var DIM_CAR_HEIGHT_CH = parseFloat($("input[name=DIM_CAR_HEIGHT_CH_MR]:checked").val());
  var DIM_HEADROOM_SH = $("[name=DIM_HEADROOM_SH_MR]");
  var DIM_CAR_BALUSTRADE_HK = parseFloat($("[name=DIM_CAR_BALUSTRADE_HK]").val());
  var DIM_TRAVEL_HEIGHT_H = parseFloat($("[name=DIM_TRAVEL_HEIGHT_H]").val());
  var MainReg = $("input:radio[name=TYP_ELEV_STANDARD]:checked").val();
  var SH_CONST = $("[name=SH_CONST]").val();
  strConstant = SH_CONST;

  if (typeof SH_CONST != "undefined") {
    if (strConstant.length < 10) {
      if (strConstant.length > 7) {
        strConstant = strConstant.substring(7, strConstant.length);
      } else {
        strConstant = strConstant.substring(6, strConstant.length);
      }
    } else {
      if (strConstant.length > 12) {
        strConstant = strConstant.substring(9, strConstant.length);
      } else {
        strConstant = strConstant.substring(8, strConstant.length);
      }
    }

    var Addition = parseFloat(SH_CONST);
  } else {
    var Addition = parseFloat(1480);
  }

  var Addition = parseFloat(strConstant);
  if (Number.isNaN(Addition)) Addition = 0; //do this as SH_CONST could be NO_MATCH_FOUND

  // First values with HK = 700 are caluculated then if HK is higher
  // the additions are done in the end

  DIM_CAR_HEIGHT_CH += Addition;

  if (DIM_CAR_BALUSTRADE_HK == 900) {
    DIM_CAR_HEIGHT_CH += 200;
  } else if (DIM_CAR_BALUSTRADE_HK == 1100) {
    DIM_CAR_HEIGHT_CH += 400;
  }

  //set interface input
  DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

  //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
  forceChangeEvent(document.ui.DIM_HEADROOM_SH_MR);
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
// GOST_33984.1-2016 added, rejsli 100419
//2019-10-03 C1-VC	Changed format to be location dependant and faster
function compRegFilter_backup() {
  //var product = $('#SELECT_ELEVATOR option:selected').val();
  var product = "N_MINISPACE_APAC"; //hard code this as is only a reference to what the product is

  var mainReg = $("input:radio[name=TYP_ELEV_STANDARD]:checked").val();

  var inputName = "ANON_USE_GRP";
  var Loc = $("#" + inputName).val();

  var regulations = [];

  // Country overrides
  switch (Loc) {
    // Switch statement example
    // case 'UK':
    // regulations['E_MONOSPACE_APAC EN81-1'] = ['None', 'EN81_70'];
    // break;

    case "ZA": // South Africa
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "UG": // Uganda
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "MA": // Morocco
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "AE": // UAE
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "KE": // Kenya
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "SEMA":
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "SA": //Saudi Arabia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "BH": //Bahrain
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "QA": //Qatar
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "OM": //Oman
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "TR": //Turkey, EN81-71 removed and EWN81-73 from EN81-1 acc to product data localization 2020-04-21 rejsli
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_72"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_72", "EN81_73"];
      break;

    case "TH": //Thailand
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "PH": //Philippines
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "VN": //Vietnam
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "ID": //Indonesia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "MY": // Malaysia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    default:
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_72"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_72", "EN81_73"];
      regulations["GOST_53780"] = ["None", "GOST_52382", "GOST_33652"];
      regulations["GOST_33984.1-2016"] = ["None", "GOST_52382", "GOST_33652"];
      //regulations['N_MONOSPACE_APAC GB7588'] = ['None', 'GB26465'];
      regulations["Pubel_2012"] = ["None"];
      regulations["Pubel_2003"] = ["None"];
      regulations["AS1735.1"] = ["None"];
      break;
  }

  var getReg = regulations[mainReg];
  applyCompRegFilter(getReg);
}

function initRegulations() {
  let releaseSelector = $("#TYP_PRODUCT_RELEASE");
  let countrySelector = $("#ANON_USE_GRP");

  if (countrySelector !== null && releaseSelector !== null) {
    //let currentCountry = countrySelector.val();
    //let currentRelease = releaseSelector.val();

    // Check to see if a back entrance has been selected, and if it has,
    // then disable the SS550:2020 option and revert back to SS550:2019.
    let backEntranceOptions = document.querySelectorAll("input[type=checkbox][id*='POS_FLR_ENTR_IN_C']");
    if (backEntranceOptions.length > 0) {
      for (let i = 0; i < backEntranceOptions.length; i++) {
        $(backEntranceOptions[i]).click(function() {
          if (hasBackdoor()) {
            disableRegulation("SS550:2020", false);

            if (document.querySelector("input[value*='SS550:2019']") !== null) {
              enableRegulation("SS550:2019", true);
            } else if (document.querySelector("input[value*='SS550:2009']") !== null) {
              enableRegulation("SS550:2009", true);
            } else if (document.querySelector("input[value*='SS550']") !== null) {
              enableRegulation("SS550", true);
            }
          } else {
            enableRegulation("SS550:2020", false);

            if (document.querySelector("input[value*='SS550:2019']") !== null) {
              enableRegulation("SS550:2019", true);
            } else if (document.querySelector("input[value*='SS550:2009']") !== null) {
              enableRegulation("SS550:2009", true);
            } else if (document.querySelector("input[value*='SS550']") !== null) {
              enableRegulation("SS550", true);
            }
          }
        });
      }
    }
  }
}

/*
 * function to set travel height range for input DIM_TRAVEL_HEIGHT_H
 */
function DIM_TRAVEL_HEIGHT_H_Range(params) {
  var DIM_TRAVEL_HEIGHT_H_Range_Total_travel = {
    //TYP_ELEV_STANDARD
    "EN81-1": {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    "EN81-20": {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    GB7588: {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    GOST_53780: {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    "GOST_33984.1-2016": {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    Pubel_2003: {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    Pubel_2012: {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    MS2021: {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    SS550: {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    "AS1735.1": {
      //QTY_CAR_ENTRANCES
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    },
    "SS550_2020": {
      1: { low: 2450, high: 160000 },
      2: { low: 2450, high: 160000 },
    }
  };

  var QTY_CAR_ENTRANCES = document.ui.QTY_CAR_ENTRANCES.value;

  if (params) {
    QTY_CAR_ENTRANCES = "1";
    if (params.checkFront && params.checkBack) {
      QTY_CAR_ENTRANCES = "2";
    }
  }

  var standard = DIM_TRAVEL_HEIGHT_H_Range_Total_travel[document.ui.TYP_ELEV_STANDARD.value][QTY_CAR_ENTRANCES];

  setGroupHeaderValues(null, true);

  document.ui["DIM_TRAVEL_HEIGHT_H_low"].value = standard.low;
  document.ui["DIM_TRAVEL_HEIGHT_H_high"].value = standard.high;
  document.ui["DIM_TRAVEL_HEIGHT_H_warninglow"].value = standard.low;
  document.ui["DIM_TRAVEL_HEIGHT_H_warninghigh"].value = standard.high;
}

function hasBackdoor() {
  let backEntranceOptions = document.querySelectorAll("input[type=checkbox][id*='POS_FLR_ENTR_IN_C']");
  if (backEntranceOptions.length > 0) {
    for (let i = 0; i < backEntranceOptions.length; i++) {
      if (backEntranceOptions[i].checked) {
        return true;
      }
    }
  }

  return false;
}

function disableRegulation(regulation, checked) {
  let option = document.querySelector("input[value*='" + regulation + "']");
  if (option !== null) {
    if (option.checked && checked !== undefined) {
      let radio = option.parentElement.querySelector("input[type=radio]");
      if (radio !== null) {
        radio.checked = checked;
        $(radio).click();
      }
    }

    $(option.parentElement).hide();
  }
}

function enableRegulation(regulation, checked) {
  let option = document.querySelector("input[value*='" + regulation + "']");
  if (option !== null) {
    $(option.parentElement).show();

    if (!option.checked && checked !== undefined) {
      let radio = option.parentElement.querySelector("input[type=radio]");
      if (radio !== null) {
        radio.checked = checked;
        $(radio).click();
      }
    }
  }
}
