//transys EU
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    minPHCalc();
    compRegFilter();
}


//transys minimum SH calculation
// Checked against KTOC R18.2 and EN81-72 2015 rule (ceiling CL88FFL 150 mm) added rejsli 2019-05-19
function minSHCalc() {
    //console.debug("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var compRegEN81_72 = false;
    var DEFAULT_HEADROOM_HEIGHT = parseFloat($('[name=DEFAULT_HEADROOM_HEIGHT]').val());
    var HEADROOM_HEIGHT = parseFloat($('[name=HEADROOM_HEIGHT]').val());
    var DIM_HEADROOM_HEIGHT_MIN = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MIN]').val());
    var DIM_HEADROOM_HEIGHT_MAX = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MAX]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var NO_HEADROOM_OPTION = $('[name=NO_HEADROOM_OPTION]').val();

    NO_HEADROOM_OPTION = 0;

    //check if EN81-72 selected
    if ($('#TYP_ELEV_STANDARD_COMPLEMENT_EN81_72').is(":checked")) {
        compRegEN81_72 = true;
    }

    if (DIM_CAR_BALUSTRADE_HK == 700) {

        if (WGT_RATED_LOAD_Q <= 2000) {
            DIM_CAR_HEIGHT_CH += 1700;
        }
        if (WGT_RATED_LOAD_Q >= 2500) {
            DIM_CAR_HEIGHT_CH = 4100;
        }
    } else {
        if (WGT_RATED_LOAD_Q <= 2000) {
            // value changed from 1800 to 1750 2020-01-30 rejsli 
            DIM_CAR_HEIGHT_CH += 1750;
        }
        if (WGT_RATED_LOAD_Q == 2500) {
            if (DIM_CAR_HEIGHT_CH <= 2300) {
                DIM_CAR_HEIGHT_CH = 4100;
            } else {
                DIM_CAR_HEIGHT_CH += 1800;
            }
        }
        if (WGT_RATED_LOAD_Q > 2500) {
            if (compRegEN81_72 == true && DIM_CAR_HEIGHT_CH > 2200) {
                DIM_CAR_HEIGHT_CH += 1900;
            } else {
                // Car height rule added 2020-01-30 rejsli 
                if (DIM_CAR_HEIGHT_CH <= 2300) {
                    DIM_CAR_HEIGHT_CH = 4100;
                } else {
                    DIM_CAR_HEIGHT_CH += 1800;
                }
            }
        }
    }

    //set interface input
    DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);


    // KONE_COUNTRY_C is an user input and cannot be used in this purpose, rejsli 060818
    // Only in NL the extension is visible in SH, 2019-08-21 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();
    var Extension = 0

    if (Loc == 'NL' || Loc == 'NL192') { //Netherlands
        var Extension = 200;
    }

    var headroom = parseFloat(DIM_HEADROOM_SH.val()) + Extension;
    DIM_HEADROOM_SH = headroom;

    document.ui["DIM_HEADROOM_SH"].value = DIM_HEADROOM_SH;

    //Low headroom
    //Default and max value to Headroom, 2020-01-29 rejsli
    //If HEADROOM_AND_PIT_SECTION = 1, visible
    if (HEADROOM_AND_PIT_SECTION == 1) {
        var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val())
        var warning = 0;

        if (WGT_RATED_LOAD_Q < 1600) {
            DIM_HEADROOM_HEIGHT_MIN = DIM_CAR_HEIGHT_CH + 1400;
        } else {
            DIM_HEADROOM_HEIGHT_MIN = DIM_CAR_HEIGHT_CH + 1550;
        }

        DEFAULT_HEADROOM_HEIGHT = DIM_HEADROOM_SH;
        DIM_HEADROOM_HEIGHT_MAX = 4500;


        if (TYP_REGULATION_EN8121 == 1) {
            warning = DEFAULT_HEADROOM_HEIGHT;
        } else {
            DIM_HEADROOM_HEIGHT_MIN = headroom;
            warning = headroom;
        }

        if (isNaN(HEADROOM_HEIGHT) || HEADROOM_HEIGHT < DIM_HEADROOM_HEIGHT_MIN || HEADROOM_HEIGHT > DIM_HEADROOM_HEIGHT_MAX) {
            HEADROOM_HEIGHT = DIM_HEADROOM_SH;
        } else {
            HEADROOM_HEIGHT = HEADROOM_HEIGHT;
        }

        //set interface inputs
        $("#DIM_HEADROOM_HEIGHT_MIN").val(DIM_HEADROOM_HEIGHT_MIN);
        $("#DIM_HEADROOM_HEIGHT_MAX").val(DIM_HEADROOM_HEIGHT_MAX);
        $("#DEFAULT_HEADROOM_HEIGHT").val(DEFAULT_HEADROOM_HEIGHT);
        $("#HEADROOM_HEIGHT").val(HEADROOM_HEIGHT);
        $("#DIM_HEADROOM_SH").val(HEADROOM_HEIGHT);
        $("#HEADROOM_HEIGHT_low").val(DIM_HEADROOM_HEIGHT_MIN);
        $("#HEADROOM_HEIGHT_high").val(DIM_HEADROOM_HEIGHT_MAX);

        if (document.getElementById('HEADROOM_HEIGHT')) {
            document.getElementById('HEADROOM_HEIGHT').setAttribute('warninglow', warning);
            updateDataTableInput(document.ui.HEADROOM_HEIGHT);
        }

        if (document.ui.NO_HEADROOM_OPTION)
            forceChangeEvent(document.ui.NO_HEADROOM_OPTION);
    }


    //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);

}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    }

    else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

//transys Pit depth 2020-01-29 rejsli
function minPHCalc() {

    var DIM_PIT_HEIGHT_PH = parseFloat($('[name=DIM_PIT_HEIGHT_PH]').val());
    var DEFAULT_PIT_DEPTH = parseFloat($('[name=DEFAULT_PIT_DEPTH]').val());
    var PIT_DEPTH = parseFloat($('[name=PIT_DEPTH]').val());
    var DIM_PIT_DEPTH_MIN = parseFloat($('[name=PIT_DEPTH_MIN]').val());
    var DIM_PIT_DEPTH_MAX = parseFloat($('[name=PIT_DEPTH_MAX]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var HEADROOM_HEIGHT = parseFloat($('[name=HEADROOM_HEIGHT]').val()); //KPT-1735
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val()); //KPT-1735
    var Warning = 0;
    var Normal = 0;
    var MinSH = 0;

    if (HEADROOM_AND_PIT_SECTION == 1) {

        //Normal pit depth EN81-20

        if (WGT_RATED_LOAD_Q <= 2000) {
            Normal = 1250;
        } else {
            if (WGT_RATED_LOAD_Q > 2000 && WGT_RATED_LOAD_Q <= 3000) {
                Normal = 1600;
            } else {
                Normal = 1750;
            }
        }
        if (WGT_RATED_LOAD_Q <= 2000) {
            DIM_PIT_DEPTH_MAX = 2000;
        } else {
            DIM_PIT_DEPTH_MAX = 2200;
        }
	 
	   //KPT-1735 Low pit possible for 1600 kg, normal headroom 2020-12-07 rejsli 
        if (TYP_REGULATION_EN8121 == 1) {
		 MinSH = DIM_CAR_HEIGHT_CH + 1750;
		 if 	(WGT_RATED_LOAD_Q == 1600 && HEADROOM_HEIGHT>= MinSH ){
           	DIM_PIT_DEPTH_MIN = 975;
		 }else{
		  	DIM_PIT_DEPTH_MIN = Normal; 
		 }
	   }else{
		DIM_PIT_DEPTH_MIN = Normal; 
        }
	 
        Warning = Normal;

        DEFAULT_PIT_DEPTH = Normal;

        if (isNaN(PIT_DEPTH) || PIT_DEPTH < DIM_PIT_DEPTH_MIN || PIT_DEPTH > DIM_PIT_DEPTH_MAX) {
            PIT_DEPTH = Normal;
        } else {
            PIT_DEPTH = PIT_DEPTH;
        }

        DIM_PIT_HEIGHT_PH = PIT_DEPTH;

        //set interface inputs
        $("#DIM_PIT_DEPTH_MIN").val(DIM_PIT_DEPTH_MIN);
        $("#DIM_PIT_DEPTH_MAX").val(DIM_PIT_DEPTH_MAX);
        $("#DEFAULT_PIT_DEPTH").val(DEFAULT_PIT_DEPTH);
        $("#PIT_DEPTH").val(PIT_DEPTH);
        $("#DIM_PIT_HEIGHT_PH").val(DIM_PIT_HEIGHT_PH);
        //set range
        $("#PIT_DEPTH_low").val(DIM_PIT_DEPTH_MIN);
        $("#PIT_DEPTH_high").val(DIM_PIT_DEPTH_MAX);
        //transys low pit and low ehadroom cannot be combined
        //document.getElementById('PIT_DEPTH').setAttribute('warninglow', Warning);  
        if (document.ui.PIT_DEPTH) {
            updateDataTableInput(document.ui.PIT_DEPTH);
        }

        //this always needs to be run last so the changes can take effect
        if (document.ui.DIM_PIT_HEIGHT_PH) {
            forceChangeEvent(document.ui.DIM_PIT_HEIGHT_PH);
        }
    }
}



// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------

/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//2019-02-20 rejsli release 18.2 GOST_33984.1-2016 added
//2019-10-01 rejsli KPT-383 LAW 13
//2019-10-03 C1-VC	Changed format to be location dependant and have
function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "TRANSYS_EUR"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();
    // NL 12-12-2018 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();

    var regulations = [];

    // Country overrides
    switch (Loc) {
        case 'BA': // Bosnia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'BG': // Bulgaria
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'CY': // Cyprus
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'EE':  // Estonia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'ES': // Spain
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'GR': // Greece
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'HR': // Croatia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'IE':  // Ireland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'IS':  // Iceland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_73'];
            break;

        case 'IT':  //Italy
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'LAW_13'];
            break;

        case 'LT': //Lithuania
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'LV': //Latvia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'ME': // Montenegro
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'MK':  // Macedonia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'NL':  //Netherlands
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'PL':  // Poland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'PT': // Portugal
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'RO': // Romania
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'RS': // Serbia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        case 'SI': //Slovenia
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'UK':  // United Kingdom
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        default:    // Defaults values if location has no match
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['GOST_53780'] = ['None', 'GOST_52382', 'GOST_33652', 'GOST_33653'];
            regulations['GOST_33984.1-2016'] = ['None', 'GOST_52382', 'GOST_33652', 'GOST_33653'];
            regulations['Pubel_2012'] = ['None'];
            break;
    }

    var getReg = regulations[mainReg];

    applyCompRegFilter(getReg);
}
