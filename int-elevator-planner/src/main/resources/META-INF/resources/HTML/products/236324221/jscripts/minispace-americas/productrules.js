//MiniSpace NA
$(function () {
    rulesInit();
});
0
function rulesInit() {
    minSHCalc();
    minPHCalc();
    compRegFilter();
}

//Minispace pit and headroom height according to DL1-00.03.007-USK
function minSHCalc() {
    //console.info("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_NA]').val());
    var VAL_RATED_SPEED_FPM = parseFloat($('[name=VAL_RATED_SPEED_FPM_NA]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_NA]').val());
    var WGT_RATED_LOAD_Q_LB = parseFloat($('[name=WGT_RATED_LOAD_Q_LB_NA]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_CAR_HEIGHT_CH_FT = parseFloat($('input[name=DIM_CAR_HEIGHT_CH_FT]:checked').val()); 
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]'); 
    var DIM_OVERHEAD_HEIGHT_H_IN = $('[name=DIM_OVERHEAD_HEIGHT_H_IN]');
    var DIM_OVERHEAD_HEIGHT_H_FT = $('[name=DIM_OVERHEAD_HEIGHT_H_FT]');
    var DIM_OVERHEAD_HEIGHT_H_COMBINED = $('[name=DIM_OVERHEAD_HEIGHT_H_COMBINED]');
    var DIM_HEADROOM_SH_EX_DISP_FT = $('[name=DIM_HEADROOM_SH_EX_DISP_FT]');
    var DIM_HEADROOM_SH_EX_DISP_IN = $('[name=DIM_HEADROOM_SH_EX_DISP_IN]');
    var DIM_HEADROOM_SH_EXT = $('[name=DIM_HEADROOM_SH_EXT]');
    var inputName = "UNIT_OF_MEASURE";
  
    var SHExt = 203;
  	

    var UNIT = $('#' + inputName).val();

    // ENA MOD - the unit lives in document.allvalues
    if (typeof UNIT === 'undefined') {
        UNIT = document.allvalues.UNIT_OF_MEASURE.value;
        //if(typeof UNIT === 'undefined') throw "product_rules UNIT_OF_MEASURE is undefined!";
    }

  
  	if (UNIT == 'METRIC') {	
	  
	  	var headroom = 0;
	  
	  	if (VAL_RATED_SPEED == 1.0){
		  if (WGT_RATED_LOAD_Q <= 1814){
		    headroom = 4292;
		  } else {
		    headroom = 4445;
		  }  
	  	}
	  	if (VAL_RATED_SPEED == 1.75){
		  if (WGT_RATED_LOAD_Q <= 1814){
		    headroom = 4445;
		  } else {
		    headroom = 4597;
		  }  
	  	}
	  	if (VAL_RATED_SPEED == 2.0){
		  if (WGT_RATED_LOAD_Q <= 1814){
		    headroom = 4547;
		  } else {
		    headroom = 4699;
		  }  
		}
	  	if (VAL_RATED_SPEED == 2.5){
		  if (WGT_RATED_LOAD_Q <= 1814){
		    headroom = 4775;
		  } else {
		    headroom = 4928;
		  }  
		}
	 
	    if (DIM_CAR_HEIGHT_CH == 2438) {
		 DIM_CAR_HEIGHT_CH = headroom;
	    }
	    if (DIM_CAR_HEIGHT_CH == 2743) {
		 DIM_CAR_HEIGHT_CH = headroom + 305;
	    }
	    if (DIM_CAR_HEIGHT_CH == 3048) {
		 DIM_CAR_HEIGHT_CH = headroom + 610;
	    }
	  
  
    } else {
	 
    	 var CH_FT = DIM_CAR_HEIGHT_CH_FT;
	 var EXT_FT = 0;
      var EXT_IN = 0;
	 	

	 if (VAL_RATED_SPEED_FPM == 200){
	   if (WGT_RATED_LOAD_Q_LB <= 4000){
		DIM_CAR_HEIGHT_CH_FT = 14;
		DIM_CAR_HEIGHT_CH = 1;
	   } else {
		DIM_CAR_HEIGHT_CH_FT = 14;
		DIM_CAR_HEIGHT_CH = 7;
	   }  
	 }
	 if (VAL_RATED_SPEED_FPM == 350){
	   if (WGT_RATED_LOAD_Q_LB <= 4000){
		DIM_CAR_HEIGHT_CH_FT = 14;
		DIM_CAR_HEIGHT_CH = 7;
	   } else {
		DIM_CAR_HEIGHT_CH_FT = 15;
		DIM_CAR_HEIGHT_CH = 1;
	   }    
	 }
	 if (VAL_RATED_SPEED_FPM == 400){
	   if (WGT_RATED_LOAD_Q_LB <= 4000){
		DIM_CAR_HEIGHT_CH_FT = 14;
		DIM_CAR_HEIGHT_CH = 11;
	   } else {
		DIM_CAR_HEIGHT_CH_FT = 15;
		DIM_CAR_HEIGHT_CH = 5;
	   }   
	 }
	 if (VAL_RATED_SPEED_FPM == 500){
	   if (WGT_RATED_LOAD_Q_LB <= 4000){
		DIM_CAR_HEIGHT_CH_FT = 15;
		DIM_CAR_HEIGHT_CH = 8;
	   } else {
		DIM_CAR_HEIGHT_CH_FT = 16;
		DIM_CAR_HEIGHT_CH = 2;
	   }   
	 }

	 if (CH_FT == 9) {
	   DIM_CAR_HEIGHT_CH_FT = DIM_CAR_HEIGHT_CH_FT + 1;
	   DIM_CAR_HEIGHT_CH = DIM_CAR_HEIGHT_CH;
	 }
	 if (CH_FT == 10) {
	   DIM_CAR_HEIGHT_CH_FT = DIM_CAR_HEIGHT_CH_FT + 2;
	   DIM_CAR_HEIGHT_CH = DIM_CAR_HEIGHT_CH;
	 }

	 if (DIM_CAR_HEIGHT_CH <=  3) {
	   EXT_FT = DIM_CAR_HEIGHT_CH_FT;
	   EXT_IN = DIM_CAR_HEIGHT_CH + 8;
	 } else {
	   EXT_FT = DIM_CAR_HEIGHT_CH_FT + 1;
	   EXT_IN = DIM_CAR_HEIGHT_CH  - 4;
	 }

	
    }

    //set interface input
    if (UNIT == 'IMP') {
        DIM_OVERHEAD_HEIGHT_H_FT.val(DIM_CAR_HEIGHT_CH_FT);
        DIM_OVERHEAD_HEIGHT_H_IN.val(DIM_CAR_HEIGHT_CH);
        DIM_HEADROOM_SH_EX_DISP_FT.val(EXT_FT);
        DIM_HEADROOM_SH_EX_DISP_IN.val(EXT_IN);

        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_FT);
        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_IN);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_FT);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_IN);
	 
	   // C1 (VC) 2019-12-16
        // Set combined display for feet and inches. 
        // Don't run this if ImperialUnit is not defined (it will break otherwise).
        if(typeof ImperialUnit === "function") {
		  DIM_OVERHEAD_HEIGHT_H_COMBINED.val(ImperialUnit.formatUnit(DIM_CAR_HEIGHT_CH_FT, DIM_CAR_HEIGHT_CH));   
            forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_COMBINED);
        }
    } else {	
	   SHExt = DIM_CAR_HEIGHT_CH + SHExt;
  	   DIM_HEADROOM_SH_EXT.val(SHExt);
	   document.ui["DIM_HEADROOM_SH"].value = DIM_CAR_HEIGHT_CH;
        forceChangeEvent(document.ui.DIM_HEADROOM_SH);
    }

 
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    } else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
function compRegFilter() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    /*var product = "MONOSPACE_500_AMER"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var regulations = [];
    regulations['MONOSPACE_500_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR GOST_53780'] = ['GOST_52382', 'GOST_33652'];
    regulations['MONOSPACE_500_EUR Pubel_2012'] = ['None'];

    var getReg = regulations[product + " " + mainReg];

    applyCompRegFilter(getReg);
    */
}
function minPHCalc() {
}
