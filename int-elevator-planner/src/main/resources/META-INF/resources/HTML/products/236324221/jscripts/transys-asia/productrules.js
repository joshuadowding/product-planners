//transys Asia
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    compRegFilter();
}


//transys minimum SH calculation
//Headroom extension calculation removed, not needed in Asian products, rejsli 231019
function minSHCalc() {
    //console.debug("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EA]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EA]').val());

    if (DIM_CAR_BALUSTRADE_HK == 700) {

        if (WGT_RATED_LOAD_Q <= 2000) {
            DIM_CAR_HEIGHT_CH += 1700;
        }
        if (WGT_RATED_LOAD_Q >= 2500) {
            DIM_CAR_HEIGHT_CH = 4100;
        }
    } else {
        if (WGT_RATED_LOAD_Q <= 2000) {
            DIM_CAR_HEIGHT_CH += 1800;
        }
        if (WGT_RATED_LOAD_Q == 2500) {
            if (DIM_CAR_HEIGHT_CH <= 2300) {
                DIM_CAR_HEIGHT_CH = 4100;
            } else {
                DIM_CAR_HEIGHT_CH += 1800;
            }
        }
        if (WGT_RATED_LOAD_Q > 2500) {
            DIM_CAR_HEIGHT_CH = 4100;
        }
    }

    //set interface input
    DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

    //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    }

    else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//2019-10-03 C1-VC	Changed format to be location dependant and faster
function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "TRANSYS_APAC"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();

    var regulations = [];

    // Country overrides
    switch (Loc) {
        // Switch statement example
        // case 'UK':
        // regulations['E_MONOSPACE_APAC EN81-1'] = ['None', 'EN81_70'];
        // break;

        default:    // Defaults values if location has no match
            regulations['EN81-1'] = ['None', 'EN81_70'];
            regulations['EN81-20'] = ['None', 'EN81_70'];
            regulations['SS550'] = ['None'];
            break;
    }

    var getReg = regulations[mainReg];

    applyCompRegFilter(getReg);
}



