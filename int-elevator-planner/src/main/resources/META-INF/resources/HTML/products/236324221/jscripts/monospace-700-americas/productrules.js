//MonoSpace 700 NA
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    minPHCalc();
    compRegFilter();
}

//MS700 minimum SH calculation acc. M700 KONE Toolbox 
// Machine height addition added + rules of CH > 8' changed KPT-1576 2020-10-21 rejsli
function minSHCalc() {
    //console.info("minSHCalc()");
    //get interface values
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_CAR_HEIGHT_CH_FT = parseFloat($('input[name=DIM_CAR_HEIGHT_CH_FT]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');  
    var TEMP_MIN_HEADROOM = parseFloat($('[name=TEMP_MIN_HEADROOM_NA]').val());
    var DIM_OVERHEAD_HEIGHT_H_IN = $('[name=DIM_OVERHEAD_HEIGHT_H_IN]');
    var DIM_OVERHEAD_HEIGHT_H_FT = $('[name=DIM_OVERHEAD_HEIGHT_H_FT]');
    var DIM_OVERHEAD_HEIGHT_H_COMBINED = $('[name=DIM_OVERHEAD_HEIGHT_H_COMBINED]');
    var DIM_HEADROOM_SH_EX_DISP_FT = $('[name=DIM_HEADROOM_SH_EX_DISP_FT]');
    var DIM_HEADROOM_SH_EX_DISP_IN = $('[name=DIM_HEADROOM_SH_EX_DISP_IN]');
    var DIM_HEADROOM_SH_EXT = $('[name=DIM_HEADROOM_SH_EXT]');
    var TEMP_MIN_OVERHEAD_FT = parseFloat($('[name=TEMP_MIN_OVERHEAD_FT]').val());
    var TEMP_MIN_OVERHEAD_IN = parseFloat($('[name=TEMP_MIN_OVERHEAD_IN]').val());
    var MAC_ADD = parseFloat($('[name=MAC_ADD]').val());
    var MAC_ADD_IN = parseFloat($('[name=MAC_ADD_IN]').val());
  
    var inputName = "UNIT_OF_MEASURE";
    //var TYP_CWT_SG_REQ = $('[name=TYP_CWT_SG_REQ]');
    //var TYP_SEISMIC_REQ = $('[name=TYP_CWT_SG_REQ]');
    //Previous do not work anymore, below added 2020-06-25 rejsli
    //var inputCWTSG = "TYP_CWT_SG_REQ";
    //var inputSEISMIC = "TYP_SEISMIC_REQ";
    //var CWTSG = $('#' + inputCWTSG ).val();
    //var SEISMIC = $('#' + inputSEISMIC).val();
  
    var SHExt = 203;
    var add = 0;
  
    var inputNameKTI = "EX1_SH_PH";
    var inputValue = $('#' + inputNameKTI).val();

    var UNIT = $('#' + inputName).val();

    // ENA MOD - the unit lives in document.allvalues
    if (typeof UNIT === 'undefined') {
        UNIT = document.allvalues.UNIT_OF_MEASURE.value;
        //if(typeof UNIT === 'undefined') throw "product_rules UNIT_OF_MEASURE is undefined!";
    }

     if (UNIT == 'METRIC') {

	   var headroom = TEMP_MIN_HEADROOM;

	   if (MAC_ADD < 305 && DIM_CAR_HEIGHT_CH == 2743) {
		add =  (305-MAC_ADD);
	   }
	   if (MAC_ADD < 610 && DIM_CAR_HEIGHT_CH == 3048) {
		add =  (610-MAC_ADD);
	   }	
	 
	   if (DIM_CAR_HEIGHT_CH == 2438) {
		DIM_CAR_HEIGHT_CH = headroom;
	   }

	   //if (CWTSG == '0' && SEISMIC == '0'){;
		if (DIM_CAR_HEIGHT_CH == 2743 || DIM_CAR_HEIGHT_CH == 3048) {
		  DIM_CAR_HEIGHT_CH = headroom + add;
		}
	  //} else {
	  //	DIM_CAR_HEIGHT_CH = headroom;
	  //}	
	  
	  //Metric extension in equation
	   
	  document.ui["DIM_HEADROOM_SH"].value = DIM_CAR_HEIGHT_CH;
  
    } else {
	 
    	 var EXT_FT = 0;
      var EXT_IN = 0;
	 
	 var headroomft = TEMP_MIN_OVERHEAD_FT;
	 var headroomin = TEMP_MIN_OVERHEAD_IN;	
	 
	 var overheadft = 0;
	 var overheadin = 0;
	 var overheadfract = 0;
	 
	 var addft = 0;
    	 var addin = 0;
	 
	 if (MAC_ADD < 305 && DIM_CAR_HEIGHT_CH_FT == 9) {
	   add = (305-MAC_ADD)/25.4;
	 }
	 if (MAC_ADD < 610 && DIM_CAR_HEIGHT_CH_FT == 10) {
	   add = (610-MAC_ADD)/25.4;
	 }
	 
      if (add == 0) {
	    addin =  headroomin;
	 } 
	 if (add > 0 && add < 12) {
	   addin = MAC_ADD_IN + headroomin;

	   if (addin > 12) {
		addft = 1;
		addin = addin - 12;
	   }
	 }
	 if (add > 12 && add < 24) {
	   addft = 1;
	   addin = MAC_ADD_IN + headroomin;

	   if (addin> 12) {
		addft = 2;
		addin = addin - 12;
	   }
	 } 
	 if (add > 24) {
	   addft = 2;
	   addin = MAC_ADD_IN + headroomin;

	   if (addin> 12) {
		addft = 3;
		addin = addin - 12;
	   }
	 }

	 if (DIM_CAR_HEIGHT_CH_FT == 8) {
		overheadft = headroomft;
		overheadin = headroomin;
	   
	   if (overheadin <= 3) {
		EXT_FT = overheadft;
		EXT_IN = overheadin + 8;
	   } else {
		EXT_FT = overheadft  + 1;
		EXT_IN = overheadin - 4;
	   }

	 } else {
		overheadft = headroomft + addft;
		overheadin = addin;
	 }
	 
	 //Inch value in fractions

	 if (DIM_CAR_HEIGHT_CH_FT == 9 || DIM_CAR_HEIGHT_CH_FT == 10) {
	   if (overheadin > 0){
		var I = overheadin.toFixed();
		var d = overheadin - I;

		switch (d) {
		  case (0.125):
		    overheadfract  = '1/8';
		    break;
		  case (0.25):  
		    overheadfract = '1/4';
		    break;
		  case (0.375):  
		    overheadfract = '3/8';
		    break;	
		  case (-0.125):  //0.875
		    overheadfract =  '7/8';
		    break;
		}
		
		if (d == 0) {
		  if (overheadin <= 3) {
		    EXT_FT = overheadft;
		    EXT_IN = overheadin + 8;
		  } else {
		    EXT_FT = overheadft  + 1;
		    EXT_IN = overheadin - 4;
		  }
		} else {
		  
		  overheadin = I + ' ' + overheadfract;
		  
		  if (I <= 3) {
		    EXT_FT = overheadft;
		    EXT_IN = (parseInt(I) + 8) + ' ' + overheadfract;
		  } else {
		    EXT_FT = overheadft  + 1;
		    if (I > 4) {
			  EXT_IN = (parseInt(I) - 4) + ' ' + overheadfract;
		    } else {  
			  EXT_IN = overheadfract;
		    }
		  }
		}			
	   }
	 }
    }

    //set interface input
    if (UNIT == 'IMP') {
        DIM_OVERHEAD_HEIGHT_H_FT.val(overheadft);
        DIM_OVERHEAD_HEIGHT_H_IN.val(overheadin);
        DIM_HEADROOM_SH_EX_DISP_FT.val(EXT_FT);
        DIM_HEADROOM_SH_EX_DISP_IN.val(EXT_IN);

        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_FT);
        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_IN);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_FT);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_IN);
	 
	   // C1 (VC) 2019-12-16
        // Set combined display for feet and inches. 
        // Don't run this if ImperialUnit is not defined (it will break otherwise).
        if(typeof ImperialUnit === "function") {
		  DIM_OVERHEAD_HEIGHT_H_COMBINED.val(ImperialUnit.formatUnit(overheadft, overheadin));   
            forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_COMBINED);
        }
    } else {	
	   SHExt = DIM_CAR_HEIGHT_CH + SHExt;
  	   DIM_HEADROOM_SH_EXT.val(SHExt);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH);
    }

 
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    } else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
function compRegFilter() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    /*var product = "MONOSPACE_500_AMER"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var regulations = [];
    regulations['MONOSPACE_500_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR GOST_53780'] = ['GOST_52382', 'GOST_33652'];
    regulations['MONOSPACE_500_EUR Pubel_2012'] = ['None'];

    var getReg = regulations[product + " " + mainReg];

    applyCompRegFilter(getReg);
    */
}
function minPHCalc() {
}
