//MonoSpace 300 NA
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    compRegFilter();
}

//MS300 minimum SH calculation acc. to DL1-02.60.001-USK_en_A.2
//Values changed acc. to KPT-1123 2020-05-26 rejsli
function minSHCalc() {
    //console.info("minSHCalc()");
    //get interface values
    //var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_NA]').val());
    //var VAL_RATED_SPEED_FPM = parseFloat($('[name=VAL_RATED_SPEED_FPM_NA]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_NA]').val());
    var WGT_RATED_LOAD_Q_LB = parseFloat($('[name=WGT_RATED_LOAD_Q_LB_NA]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_CAR_HEIGHT_CH_FT = parseFloat($('input[name=DIM_CAR_HEIGHT_CH_FT]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_OVERHEAD_HEIGHT_H_IN = $('[name=DIM_OVERHEAD_HEIGHT_H_IN]');
    var DIM_OVERHEAD_HEIGHT_H_FT = $('[name=DIM_OVERHEAD_HEIGHT_H_FT]');
    var DIM_OVERHEAD_HEIGHT_H_COMBINED = $('[name=DIM_OVERHEAD_HEIGHT_H_COMBINED]');
    var DIM_HEADROOM_SH_EX_DISP_FT = $('[name=DIM_HEADROOM_SH_EX_DISP_FT]');
    var DIM_HEADROOM_SH_EX_DISP_IN = $('[name=DIM_HEADROOM_SH_EX_DISP_IN]');
    var inputName = "UNIT_OF_MEASURE";

    var UNIT = $('#' + inputName).val();

    // ENA MOD - the unit lives in document.allvalues
    if (typeof UNIT === 'undefined') {
        UNIT = document.allvalues.UNIT_OF_MEASURE.value;
        //if(typeof UNIT === 'undefined') throw "product_rules UNIT_OF_MEASURE is undefined!";
    }

    var EXT_FT = 0;
    var EXT_IN = 0;

    //Copied from EcoSpace 2021-03-26 rejsli
    if (UNIT == 'METRIC') {
        DIM_CAR_HEIGHT_CH = 3962;
    } else {
        DIM_CAR_HEIGHT_CH_FT = 13;
        DIM_CAR_HEIGHT_CH = 0;
        EXT_FT = 13;
        EXT_IN = 8;
    }

    //set interface input
    if (UNIT == 'IMP') {
        DIM_OVERHEAD_HEIGHT_H_FT.val(DIM_CAR_HEIGHT_CH_FT);
        DIM_OVERHEAD_HEIGHT_H_IN.val(DIM_CAR_HEIGHT_CH);
        DIM_HEADROOM_SH_EX_DISP_FT.val(EXT_FT);
        DIM_HEADROOM_SH_EX_DISP_IN.val(EXT_IN);

        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_FT);
        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_IN);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_FT);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_IN);
	 
	   // C1 (VC) 2019-12-16
	   // Set combined display for feet and inches. 
        // Don't run this if ImperialUnit is not defined (it will break otherwise).
        if(typeof ImperialUnit === "function") {
          DIM_OVERHEAD_HEIGHT_H_COMBINED.val(ImperialUnit.formatUnit(DIM_CAR_HEIGHT_CH_FT, DIM_CAR_HEIGHT_CH));   
		forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_COMBINED);
        }
    } else {
        DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH);
    }
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    } else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}


// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
function compRegFilter() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    /*var product = "MONOSPACE_500_AMER"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var regulations = [];
    regulations['MONOSPACE_500_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR GOST_53780'] = ['GOST_52382', 'GOST_33652'];
    regulations['MONOSPACE_500_EUR Pubel_2012'] = ['None'];

    var getReg = regulations[product + " " + mainReg];

    applyCompRegFilter(getReg);
    */
}
