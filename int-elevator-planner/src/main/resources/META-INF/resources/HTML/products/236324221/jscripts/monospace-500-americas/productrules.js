//MonoSpace 500 NA
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    compRegFilter();
}

//MS500 minimum SH calculation acc. to DL1-02.26.004-USK D.1
//Metric values checked 0.75 and 1.0 m/s, load > 1158 2020-05-27 rejsli
//Metric values set in imperial case for BIM KPT-2207 2021-05-18 rejsli
function minSHCalc() {
    //console.info("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_NA]').val());
    var VAL_RATED_SPEED_FPM = parseFloat($('[name=VAL_RATED_SPEED_FPM_NA]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_NA]').val());
    var WGT_RATED_LOAD_Q_LB = parseFloat($('[name=WGT_RATED_LOAD_Q_LB_NA]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_CAR_HEIGHT_CH_FT = parseFloat($('input[name=DIM_CAR_HEIGHT_CH_FT]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_OVERHEAD_HEIGHT_H_IN = $('[name=DIM_OVERHEAD_HEIGHT_H_IN]');
    var DIM_OVERHEAD_HEIGHT_H_FT = $('[name=DIM_OVERHEAD_HEIGHT_H_FT]');
    var DIM_OVERHEAD_HEIGHT_H_COMBINED = $('[name=DIM_OVERHEAD_HEIGHT_H_COMBINED]');
    var DIM_HEADROOM_SH_EX_DISP_FT = $('[name=DIM_HEADROOM_SH_EX_DISP_FT]');
    var DIM_HEADROOM_SH_EX_DISP_IN = $('[name=DIM_HEADROOM_SH_EX_DISP_IN]');
    var DIM_HEADROOM_SH_EXT = $('[name=DIM_HEADROOM_SH_EXT]');
    var inputName = "UNIT_OF_MEASURE";
    var SHExt = 203;

    var UNIT = $('#' + inputName).val();

    // ENA MOD - the unit lives in document.allvalues
    if (typeof UNIT === 'undefined') {
        UNIT = document.allvalues.UNIT_OF_MEASURE.value;
        //if(typeof UNIT === 'undefined') throw "product_rules UNIT_OF_MEASURE is undefined!";
    }

    var EXT_FT = 0;
    var EXT_IN = 0;
    var TEMP_MIN = 0; //KPT-2207
    var EXT = 0; //KPT-2207

    if (UNIT == 'METRIC') {
	  if (VAL_RATED_SPEED == 0.75 && WGT_RATED_LOAD_Q <= 1588) {
            if (DIM_CAR_HEIGHT_CH == 2438) {
                DIM_CAR_HEIGHT_CH = 3962;
            }
            if (DIM_CAR_HEIGHT_CH == 2743) {
                DIM_CAR_HEIGHT_CH = 4267;
            }
            if (DIM_CAR_HEIGHT_CH == 3048) {
                DIM_CAR_HEIGHT_CH = 4572;
            }
        }
        if (VAL_RATED_SPEED == 0.75 && WGT_RATED_LOAD_Q > 1588) {
            if (DIM_CAR_HEIGHT_CH == 2438) {
                DIM_CAR_HEIGHT_CH = 4147;
            }
            if (DIM_CAR_HEIGHT_CH == 2743) {
                DIM_CAR_HEIGHT_CH = 4451;
            }
            if (DIM_CAR_HEIGHT_CH == 3048) {
                DIM_CAR_HEIGHT_CH = 4756;
            }
        }
        if (VAL_RATED_SPEED == 1.0 && WGT_RATED_LOAD_Q <= 1588) {
            if (DIM_CAR_HEIGHT_CH == 2438) {
                DIM_CAR_HEIGHT_CH = 4064;
            }
            if (DIM_CAR_HEIGHT_CH == 2743) {
                DIM_CAR_HEIGHT_CH = 4369;
            }
            if (DIM_CAR_HEIGHT_CH == 3048) {
                DIM_CAR_HEIGHT_CH = 4674;
            }
        }
        if (VAL_RATED_SPEED == 1.0 && WGT_RATED_LOAD_Q > 1588) {
            if (DIM_CAR_HEIGHT_CH == 2438) {
                DIM_CAR_HEIGHT_CH = 4147;
            }
            if (DIM_CAR_HEIGHT_CH == 2743) {
                DIM_CAR_HEIGHT_CH = 4451;
            }
            if (DIM_CAR_HEIGHT_CH == 3048) {
                DIM_CAR_HEIGHT_CH = 4756;
            }
        }
        if (VAL_RATED_SPEED == 1.75 && WGT_RATED_LOAD_Q <= 1588) {
            if (DIM_CAR_HEIGHT_CH == 2438) {
                DIM_CAR_HEIGHT_CH = 4102;
            }
            if (DIM_CAR_HEIGHT_CH == 2743) {
                DIM_CAR_HEIGHT_CH = 4407;
            }
            if (DIM_CAR_HEIGHT_CH == 3048) {
                DIM_CAR_HEIGHT_CH = 4711;
            }
        }
        if (VAL_RATED_SPEED == 1.75 && WGT_RATED_LOAD_Q > 1588) {
            if (DIM_CAR_HEIGHT_CH == 2438) {
                DIM_CAR_HEIGHT_CH = 4166;
            }
            if (DIM_CAR_HEIGHT_CH == 2743) {
                DIM_CAR_HEIGHT_CH = 4470;
            }
            if (DIM_CAR_HEIGHT_CH == 3048) {
                DIM_CAR_HEIGHT_CH = 4775;
            }
        }
    } else { 
        if (VAL_RATED_SPEED_FPM == 150 && WGT_RATED_LOAD_Q_LB <= 3500) {
            if (DIM_CAR_HEIGHT_CH_FT == 8) {
                DIM_CAR_HEIGHT_CH_FT = 13;
                DIM_CAR_HEIGHT_CH = 0;
                EXT_FT = 13;
                EXT_IN = 8;
		      TEMP_MIN = 3962;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 9) {
                DIM_CAR_HEIGHT_CH_FT = 14;
                DIM_CAR_HEIGHT_CH = 0;
                EXT_FT = 14;
                EXT_IN = 8;
			 TEMP_MIN = 4267;	  
            }
            if (DIM_CAR_HEIGHT_CH_FT == 10) {
                DIM_CAR_HEIGHT_CH_FT = 15;
                DIM_CAR_HEIGHT_CH = 0;
                EXT_FT = 15;
                EXT_IN = 8;
		      TEMP_MIN = 4572;
            }
        }
        if (VAL_RATED_SPEED_FPM == 150 && WGT_RATED_LOAD_Q_LB > 3500) {
            if (DIM_CAR_HEIGHT_CH_FT == 8) {
                DIM_CAR_HEIGHT_CH_FT = 13;
                DIM_CAR_HEIGHT_CH = "7 1/4";
                EXT_FT = 14;
                EXT_IN = "3 1/4";
		      TEMP_MIN = 4147;
		      
            }
            if (DIM_CAR_HEIGHT_CH_FT == 9) {
                DIM_CAR_HEIGHT_CH_FT = 14;
                DIM_CAR_HEIGHT_CH = "7 1/4";
                EXT_FT = 15;
                EXT_IN = "3 1/4";
		      TEMP_MIN = 4451;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 10) {
                DIM_CAR_HEIGHT_CH_FT = 15;
                DIM_CAR_HEIGHT_CH = "7 1/4";
                EXT_FT = 16;
                EXT_IN = "3 1/4";
		      TEMP_MIN = 4756;
            }
        }
        if (VAL_RATED_SPEED_FPM == 200 && WGT_RATED_LOAD_Q_LB <= 3500) {
            if (DIM_CAR_HEIGHT_CH_FT == 8) {
                DIM_CAR_HEIGHT_CH_FT = 13;
                DIM_CAR_HEIGHT_CH = 4;
                EXT_FT = 14;
                EXT_IN = 0;
		      TEMP_MIN = 4064;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 9) {
                DIM_CAR_HEIGHT_CH_FT = 14;
                DIM_CAR_HEIGHT_CH = 4;
                EXT_FT = 15;
                EXT_IN = 0;
		      TEMP_MIN = 4369;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 10) {
                DIM_CAR_HEIGHT_CH_FT = 15;
                DIM_CAR_HEIGHT_CH = 4;
                EXT_FT = 16;
                EXT_IN = 0;
		      TEMP_MIN = 4674;
            }
        }
        if (VAL_RATED_SPEED_FPM == 200 && WGT_RATED_LOAD_Q_LB > 3500) {
            if (DIM_CAR_HEIGHT_CH_FT == 8) {
                DIM_CAR_HEIGHT_CH_FT = 13;
                DIM_CAR_HEIGHT_CH = "7 1/4";
                EXT_FT = 14;
                EXT_IN = "3 1/4";
		      TEMP_MIN = 4147;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 9) {
                DIM_CAR_HEIGHT_CH_FT = 14;
                DIM_CAR_HEIGHT_CH = "7 1/4";
                EXT_FT = 15;
                EXT_IN = "3 1/4";
		      TEMP_MIN = 4451;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 10) {
                DIM_CAR_HEIGHT_CH_FT = 15;
                DIM_CAR_HEIGHT_CH = "7 1/4";
                EXT_FT = 16;
                EXT_IN = "3 1/4";
		      TEMP_MIN = 4756;
            }
        }
        if (VAL_RATED_SPEED_FPM == 350 && WGT_RATED_LOAD_Q_LB <= 3500) {
            if (DIM_CAR_HEIGHT_CH_FT == 8) {
                DIM_CAR_HEIGHT_CH_FT = 13;
                DIM_CAR_HEIGHT_CH = "5 1/2";
                EXT_FT = 14;
                EXT_IN = "1 1/2";
		      TEMP_MIN = 4102;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 9) {
                DIM_CAR_HEIGHT_CH_FT = 14;
                DIM_CAR_HEIGHT_CH = "5 1/2";
                EXT_FT = 15;
                EXT_IN = "1 1/2";
		      TEMP_MIN = 4407;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 10) {
                DIM_CAR_HEIGHT_CH_FT = 15;
                DIM_CAR_HEIGHT_CH = "5 1/2";
                EXT_FT = 16;
                EXT_IN = "1 1/2";
		      TEMP_MIN = 4711;
            }
        }
        if (VAL_RATED_SPEED_FPM == 350 && WGT_RATED_LOAD_Q_LB > 3500) {
            if (DIM_CAR_HEIGHT_CH_FT == 8) {
                DIM_CAR_HEIGHT_CH_FT = 13;
                DIM_CAR_HEIGHT_CH = 8;
                EXT_FT = 14;
                EXT_IN = 4;
		      TEMP_MIN = 4166;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 9) {
                DIM_CAR_HEIGHT_CH_FT = 14;
                DIM_CAR_HEIGHT_CH = 8;
                EXT_FT = 15;
                EXT_IN = 4;
		      TEMP_MIN = 4470;
            }
            if (DIM_CAR_HEIGHT_CH_FT == 10) {
                DIM_CAR_HEIGHT_CH_FT = 15;
                DIM_CAR_HEIGHT_CH = 8;
                EXT_FT = 16;
                EXT_IN = 4;
		      TEMP_MIN = 4775;
            }
        }
	  EXT = TEMP_MIN + SHExt; //KPT-2181
    }
 

    //set interface input
    if (UNIT == 'IMP') {
        DIM_OVERHEAD_HEIGHT_H_FT.val(DIM_CAR_HEIGHT_CH_FT);
        DIM_OVERHEAD_HEIGHT_H_IN.val(DIM_CAR_HEIGHT_CH);
        DIM_HEADROOM_SH_EX_DISP_FT.val(EXT_FT);
        DIM_HEADROOM_SH_EX_DISP_IN.val(EXT_IN);
	   DIM_HEADROOM_SH.val(TEMP_MIN); //KPT-2207
	   DIM_HEADROOM_SH_EXT.val(EXT) //KPT-2207

        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_FT);
        forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_IN);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_FT);
        forceChangeEvent(document.ui.DIM_HEADROOM_SH_EX_DISP_IN);
	   forceChangeEvent(document.ui.DIM_HEADROOM_SH); //KPT-2207
	   forceChangeEvent(document.ui.DIM_HEADROOM_SH_EXT); //KPT-2207
	 
	   // C1 (VC) 2019-12-16
        // Set combined display for feet and inches. 
        // Don't run this if ImperialUnit is not defined (it will break otherwise).
        if(typeof ImperialUnit === "function") {
		  DIM_OVERHEAD_HEIGHT_H_COMBINED.val(ImperialUnit.formatUnit(DIM_CAR_HEIGHT_CH_FT, DIM_CAR_HEIGHT_CH));   
            forceChangeEvent(document.ui.DIM_OVERHEAD_HEIGHT_H_COMBINED);
        }
    } else {
	   SHExt = DIM_CAR_HEIGHT_CH + SHExt;
  	   DIM_HEADROOM_SH_EXT.val(SHExt);
        DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);
        	forceChangeEvent(document.ui.DIM_HEADROOM_SH);
    }

}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    } else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
function compRegFilter() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    /*var product = "MONOSPACE_500_AMER"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var regulations = [];
    regulations['MONOSPACE_500_EUR EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
    regulations['MONOSPACE_500_EUR GOST_53780'] = ['GOST_52382', 'GOST_33652'];
    regulations['MONOSPACE_500_EUR Pubel_2012'] = ['None'];

    var getReg = regulations[product + " " + mainReg];

    applyCompRegFilter(getReg);
    */
}
