//EMonoSpace
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    compRegFilter();
}

function minPHCalc() {
  // Doesn't do anything. Prevents breaking filters as this function was attached
  // directly to inputs. 
  return;
}


//EMonoSpace minimum SH calculation acc to KTOC rules release 18.1, rejsli 270618
//Headroom extension calculation removed, not needed in Asian Products, rejsli 231019
// no need for sepearate rules
function minSHCalc() {
    //console.debug("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EA]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());


    // First values with HK = 700 are caluculated then if HK is higher
    // the additions are done in the end


    if (VAL_RATED_SPEED == 1.0) {
        DIM_CAR_HEIGHT_CH += 1380;
    }
    if (VAL_RATED_SPEED == 1.6) {
        DIM_CAR_HEIGHT_CH += 1570;
    }
    if (VAL_RATED_SPEED == 1.75) {
        DIM_CAR_HEIGHT_CH += 1620;
    }
    if (DIM_CAR_HEIGHT_CH == 2100) {
        DIM_CAR_HEIGHT_CH += 100;
    }

    if (DIM_CAR_BALUSTRADE_HK == 900) {
        DIM_CAR_HEIGHT_CH += 200;
    }
    if (DIM_CAR_BALUSTRADE_HK == 1100) {
        DIM_CAR_HEIGHT_CH += 400;
    }


    //set interface input
    DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

    //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//Accoring to KTOC R18.1, rejsli 210618
//GOST_33984.1-2016 added, rejsli 100419
//2019-10-03 C1-VC	Changed format to be location dependant and have

function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "E_MONOSPACE_APAC"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();

    var regulations = [];

    // Country overrides
    switch (Loc) {
        // Switch statement example
        // case 'UK':
            // regulations['E_MONOSPACE_APAC EN81-1'] = ['None', 'EN81_70'];
            // break;
	   
	   case 'ZA': // South Africa
	   	  regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;	
	   
	   case 'UG': // Uganda
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;	
	   
	   case 'MA': // Morocco
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'AE': // UAE
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;	
	   
	   case 'KE': // Kenya
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'SEMA':
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'SA': //Saudi Arabia
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'BH': //Bahrain
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'QA': //Qatar
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'OM': //Oman
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'TR': //Turkey, EN81-71, EN81-72 from both and EN81-73 removed from EN81-1 acc to product data localization 2020-04-21 rejsli
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_77_TURKEY'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_73'];
            break;
	   
	   case 'TH': //Thailand
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;
	   
	   case 'PH': //Philippines
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;
	   
	   case 'VN': //Vietnam
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;
	   
	   case 'ID': //Indonesia
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;
	   
	   case 'MY': //Malaysia
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;
	   
        default:
            regulations['EN81-1'] = ['None', 'EN81_70'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_73'];
            regulations['GOST_53780'] = ['None', 'GOST_52382', 'GOST_33652'];
            regulations['GOST_33984.1-2016'] = ['None', 'GOST_52382', 'GOST_33652'];
            //regulations['GB7588'] = ['None']; commected 2020-02-28 rejsli, not used
            regulations['Pubel_2012'] = ['None'];
	   	  regulations['MS2021'] = ['None'];
	       regulations['SS550'] = ['None'];
	       regulations['AS1735.1_EN81-1'] = ['None', 'None'];
            regulations['AS1735.1_EN81-20'] = ['None', 'None', 'None'];
            break;
    }

    /* regulations['E_MONOSPACE_APAC EN81-1'] = ['None', 'EN81_70'];
    regulations['E_MONOSPACE_APAC EN81-20'] = ['None', 'EN81_70', 'EN81_73'];
    regulations['E_MONOSPACE_APAC GOST_53780'] = ['None', 'GOST_52382', 'GOST_33652'];
    regulations['E_MONOSPACE_APAC GOST_33984.1-2016'] = ['None', 'GOST_52382', 'GOST_33652'];
    regulations['E_MONOSPACE_APAC GB7588'] = ['None'];
    regulations['E_MONOSPACE_APAC Pubel_2012'] = ['None']; */

    var getReg = regulations[mainReg];

    applyCompRegFilter(getReg);
}
