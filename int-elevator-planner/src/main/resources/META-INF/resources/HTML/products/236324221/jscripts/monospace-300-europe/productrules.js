//MonoSpace 300
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    minPHCalc();
    compRegFilter();
}

//MS300 minimum SH calculation
function minSHCalc() {
    //console.info("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var compRegEN81_72 = false;
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());
    var DEFAULT_HEADROOM_HEIGHT = parseFloat($('[name=DEFAULT_HEADROOM_HEIGHT]').val());
    var HEADROOM_HEIGHT = parseFloat($('[name=HEADROOM_HEIGHT]').val());
    var DIM_HEADROOM_HEIGHT_MIN = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MIN]').val());
    var DIM_HEADROOM_HEIGHT_MAX = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MAX]').val());
    var TEMP_MIN_HEADROOM = parseFloat($('[name=TEMP_MIN_HEADROOM]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var TYP_LDO_FRAME_FRONT = $('[name=TYP_LDO_FRAME_FRONT]').val();
    var TYP_LOW_HEADROOM_OPTION = $('[name=TYP_LOW_HEADROOM_OPTION]').val();
    var NO_HEADROOM_OPTION = $('[name=NO_HEADROOM_OPTION]').val();
    var DIM_DOOR_LL_A = parseFloat($('input[name=DIM_DOOR_LL_A_EU]:checked').val())
    var KES202 = parseFloat($('[name=KES202_IN_USE]').val()); //KPT-2028 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();
  
    NO_HEADROOM_OPTION = 0;

    if (DIM_CAR_BALUSTRADE_HK == 700) {
        if  (Loc != "DK") { //KPT-2190 DK defaults 2021-05-10 rejsli
      	DIM_CAR_HEIGHT_CH += 1300;
	   } else {
	   	DIM_CAR_HEIGHT_CH += 1400;
	   }	 
    } else {  			
        DIM_CAR_HEIGHT_CH += 1700;
    }

    //set interface input
    DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

    // Only in NL the extension is visible in SH, 2019-08-21 rejsli

    var Extension = 0

    if (Loc == 'NL' || Loc == 'NL192') { //Netherlands
        var Extension = 200;
    }

    var headroom = parseFloat(DIM_HEADROOM_SH.val()) + Extension;
    DIM_HEADROOM_SH = headroom;
    
    document.ui["DIM_HEADROOM_SH"].value = DIM_HEADROOM_SH;

  
    //Low headroom
    //Default and max value to Headroom, 2020-01-17 rejsli
    //If HEADROOM_AND_PIT_SECTION = 1, visible
    if (HEADROOM_AND_PIT_SECTION == 1) {
        var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val())
        var Add = 0;
        var warning = 0;

	   //If LOW or NO heardoom are possible
        //NO headroom needs KES202 and that is only available as frame and if other counties come only to CH and DE
	   //R20.2 KES202 in all cases
	   if (TYP_LOW_HEADROOM_OPTION != "0") {    	
	 	if (TYP_LOW_HEADROOM_OPTION == "NO") {
            	if (TYP_LDO_FRAME_FRONT != "FRAME" || KES202 == 1) {
               	TEMP_MIN_HEADROOM = 2600;
            	}
        	}

        	if (DIM_CAR_HEIGHT_CH == 2200) {
            	var Add = 100;
        	}
        	var Minimum = TEMP_MIN_HEADROOM + Add;
		
	   }else{
	   	TEMP_MIN_HEADROOM = headroom;
	   }

        DEFAULT_HEADROOM_HEIGHT = DIM_HEADROOM_SH;
        DIM_HEADROOM_HEIGHT_MAX = 4200;


        // Travel restriction	
        if (DIM_TRAVEL_HEIGHT_H >= 30000 && WGT_RATED_LOAD_Q == 1000) {
            TYP_REGULATION_EN8121 = 0;
        }

        if (TYP_REGULATION_EN8121 == 1) {
            DIM_HEADROOM_HEIGHT_MIN = Minimum;
            warning = DEFAULT_HEADROOM_HEIGHT;
        } else {
            DIM_HEADROOM_HEIGHT_MIN = headroom;
		  warning = headroom;
        }

        if (isNaN(HEADROOM_HEIGHT) || HEADROOM_HEIGHT < DIM_HEADROOM_HEIGHT_MIN || HEADROOM_HEIGHT > DIM_HEADROOM_HEIGHT_MAX) {
            HEADROOM_HEIGHT = DIM_HEADROOM_SH;
        } else {
            HEADROOM_HEIGHT = HEADROOM_HEIGHT;
        }
	 
	  //KPT-717 M300 minimum is 2600, to utilize KES202, rules changed
	  //if (HEADROOM_HEIGHT < 2600){
	  //R20.2 KES202 in all cases -> no headroom option not needed
	  if (KES202 == 0) {
	    if (HEADROOM_HEIGHT == 2600 && TYP_LOW_HEADROOM_OPTION == "NO" && DIM_DOOR_LL_A < 1000 && TYP_LDO_FRAME_FRONT == "FRAME"){
		  NO_HEADROOM_OPTION = 1;
	    }else{ 
		  NO_HEADROOM_OPTION = 0;
	    }
	  }else{ 
	  	NO_HEADROOM_OPTION = 0;
	  }

        //set interface inputs
        document.ui["DIM_HEADROOM_HEIGHT_MIN"].value = DIM_HEADROOM_HEIGHT_MIN;
        document.ui["DIM_HEADROOM_HEIGHT_MAX"].value = DIM_HEADROOM_HEIGHT_MAX;
        document.ui["DEFAULT_HEADROOM_HEIGHT"].value = DEFAULT_HEADROOM_HEIGHT;
        document.ui["HEADROOM_HEIGHT"].value = HEADROOM_HEIGHT;
        document.ui["DIM_HEADROOM_SH"].value = HEADROOM_HEIGHT;
        document.ui["HEADROOM_HEIGHT_low"].value = DIM_HEADROOM_HEIGHT_MIN;
        document.ui["HEADROOM_HEIGHT_high"].value = DIM_HEADROOM_HEIGHT_MAX;
	   document.ui["NO_HEADROOM_OPTION"].value = NO_HEADROOM_OPTION;
	 
  	   document.getElementById('HEADROOM_HEIGHT').setAttribute('warninglow', warning);
	   updateDataTableInput(document.ui.HEADROOM_HEIGHT);
    }	 

    //this always needs to be run last so the DIM_HEADROOM_SH changes can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);
    
    if(document.ui.NO_HEADROOM_OPTION)
        forceChangeEvent(document.ui.NO_HEADROOM_OPTION);
  
}

//M300 Pit depth 2020-01-16 rejsli
function minPHCalc() {

    var DIM_PIT_HEIGHT_PH = parseFloat($('[name=DIM_PIT_HEIGHT_PH]').val());
    var DEFAULT_PIT_DEPTH = parseFloat($('[name=DEFAULT_PIT_DEPTH]').val());
    var PIT_DEPTH = parseFloat($('[name=PIT_DEPTH]').val());
    var DIM_PIT_DEPTH_MIN = parseFloat($('[name=PIT_DEPTH_MIN]').val());
    var DIM_PIT_DEPTH_MAX = parseFloat($('[name=PIT_DEPTH_MAX]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var Warning = 0;
    var Normal = 0;

    if (HEADROOM_AND_PIT_SECTION == 1) {
	 
	   //Normal pit depth EN81-20

	   if (WGT_RATED_LOAD_Q == 320){
		Normal = 1300;
	   }else{
		Normal = 1100;
	   }		

        DIM_PIT_DEPTH_MAX = 1400;

        // Travel restrictions	
        if (DIM_TRAVEL_HEIGHT_H >= 30000 && WGT_RATED_LOAD_Q >= 1000) {
            TYP_REGULATION_EN8121 = 0;
        }

        DIM_PIT_DEPTH_MIN = Normal;
	   Warning = Normal;

        DEFAULT_PIT_DEPTH = Normal;

        if (isNaN(PIT_DEPTH) || PIT_DEPTH < DIM_PIT_DEPTH_MIN || PIT_DEPTH > DIM_PIT_DEPTH_MAX) {
            PIT_DEPTH = Normal;
        } else {
            PIT_DEPTH = PIT_DEPTH;
        }

	   DIM_PIT_HEIGHT_PH = PIT_DEPTH;
	 
        //set interface inputs
        document.ui["DIM_PIT_DEPTH_MIN"].value = DIM_PIT_DEPTH_MIN;
        document.ui["DIM_PIT_DEPTH_MAX"].value = DIM_PIT_DEPTH_MAX;
        document.ui["DEFAULT_PIT_DEPTH"].value = DEFAULT_PIT_DEPTH;
        document.ui["PIT_DEPTH"].value = PIT_DEPTH;
        document.ui["DIM_PIT_HEIGHT_PH"].value = DIM_PIT_HEIGHT_PH;
        //set range
        document.ui["PIT_DEPTH_low"].value = DIM_PIT_DEPTH_MIN;
        document.ui["PIT_DEPTH_high"].value = DIM_PIT_DEPTH_MAX;
	   //M300 does not have low pit option	 
	   //document.getElementById('PIT_DEPTH').setAttribute('warninglow', Warning);  
	   updateDataTableInput(document.ui.PIT_DEPTH);
	
    	   //this always needs to be run last so the changes can take effect
        forceChangeEvent(document.ui.DIM_PIT_HEIGHT_PH);
    }
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    } else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//2019-08-16 rejsli release 19.2
//2019-09-25 rejsli KPT-383 LAW 13
//2019-10-03 C1-VC	Changed format to be location dependant and have faster access.
//2020-02-17 rejsli IT + Law 13 added back
function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "MONOSPACE_300_EUR"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();
    var Extension = 0

    var regulations = [];

    // Country overrides
    switch (Loc) {
	   
	   
	   case 'IT':  // Italy;
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77', 'LAW_13'];
	   break;

        default:     // Defaults values if location has no match
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_73', 'EN81_77'];
            break;
    }

    var getReg = regulations[mainReg];

    applyCompRegFilter(getReg);
}


