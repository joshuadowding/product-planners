// products\236324221\jscripts\regulations_module.js

var Regulations = (function () {
    // --------------------------------------------------------------------
    // Debugging section
    // --------------------------------------------------------------------
    const conf = {
        // Configureation
        moduleName: 'Regulations Module',
        get logPrefix() { return this.moduleName + ' =>' },

        // Flags
        debug: false,
        foundRules: false,
        disableCombinations: false,

        // Input name patterns
        mainRegSuffix: 'MAIN_REG_',
        input_regRules: 'RULES_',
        input_mainReg: 'TYP_ELEV_STANDARD',
        input_compReg: 'TYP_ELEV_STANDARD_COMPLEMENT',
        input_regMatrix: 'TYP_ELEV_STANDARD_COMPLEMENT_MATRIX',

        // Setting this and running init will add a once time event to the group header
        regulationsGroupSelector: 'div[id^=collapse256245441]',

        // Values:
        matrix_no_match: 'NONE',

        // Helpers
        get rulesInput() {
            const input = document.querySelector(`select[id^=${this.input_regRules}]`);
            this.foundRules = input ? true : false;
            return input;
        },

        get product() {
            // Select product name
            const selector = document.getElementById('SELECT_ELEVATOR');
            return selector.options[selector.selectedIndex].value;
        },

        get selectedMainReg() {
            // Select primary regulation
            const input = document.querySelector(`input[name=${this.input_mainReg}]:checked`);
            return input ? input.value : null;
        },

        get selectedCompRegs() {
            // Select secondary regulation
            return document.querySelectorAll(`input[name^=${this.input_compReg}][type=checkbox]:checked`);
        },

        get allCompRegs() {
            // All secondary regulation
            return document.querySelectorAll(`input[name^=${this.input_compReg}][type=checkbox]`);
        },
        get visibleCompReg() {
            // Currently unhidden secondary regulation
            const visible = [...this.allCompRegs].filter(checkbox => {
                if (checkbox.closest('div').style.display != 'none') {
                    return checkbox;
                }
            });

            return visible;
        },

        get hasBackdoor() {
            return document.querySelectorAll('input[id^=POS_FLR_ENTR_IN_C_][type=checkbox]:checked').length > 0;
        },

        get hasFrontdoor() {
            return document.querySelectorAll('input[id^=POS_FLR_ENTR_IN_A_][type=checkbox]:checked').length > 0;
        },

        get frontdoorsOnly() {
            return this.hasFrontdoor && !this.hasBackdoor;
        }
    };

    function log() {
        // https://gist.github.com/robatron/5681424#modifying-log-messages
        // TODO: implement this instead
        // https://stackoverflow.com/questions/13815640/a-proper-wrapper-for-console-log-with-correct-line-number
        if (conf.debug) {
            const args = Array.prototype.slice.call(arguments);     // 1. Convert args to a normal array
            args.unshift(conf.logPrefix);                           // 2. Prepend log prefix log string
            console.log.apply(console, args);                       // 3. Pass along arguments to console.log
        }
    }

    // --------------------------------------------------------------------
    // Module section
    // --------------------------------------------------------------------
    let rules = [];
    let initialized = false;

    function getRegRules() {
        log('Initialising' + conf.moduleName + '...');

        // Shorten method calls
        const input = conf.rulesInput;

        // Only run if input was found
        if (input) {
            if (input.options.length > 0) {
                const option = input.options[input.selectedIndex].value;
                const id = input.id;

                log(input, option, id);

                if (!input || !option || !id) {
                    console.debug("Some parameters are missing!");
                } else {
                    // Get data and run first update
                    getValueAttributes(id, option, processRegulationsRules);
                    updateDisplay();

                    switch (option) {
                        case "IE":
                        case "UK":
                            let en81_70 = document.getElementById("TYP_ELEV_STANDARD_COMPLEMENT_EN81_70");
                            if (en81_70 !== null) en81_70.checked = true;
                            break;
                    }
                }
            }
        } else {
            console.warn('Regulation rules input or option not found, skipping execution...');
        }
    }

    // Restructure data
    function processRegulationsRules(options) {
        // Reset status
        const tmp = [];
        initialized = false;

        try {
            log(options);

            // Map as [Main regulation] = [complementary 1, complementary 2, ...];
            for (const main of options[0].result) {
                if (main.value) {
                    const mainReg = main.attr.replace(conf.mainRegSuffix, '');
                    tmp[mainReg] = main.value;
                }
            }
        } catch (error) {
            console.error(error);
            rules = [];
            return false;
        }

        log('Rules', tmp);

        initialized = true;
        rules = tmp;

        return true;
    }

    // function updateDisplay() {
    //     if(typeof RunAfterDrivens === undefined) {
    //         console.warn('RunAfterDrivens (designUI) is undefined. Definitions might be out of sync.')
    //     }
    //     RunAfterDrivens('updateReg', 250, () => {
    //         updateDisplay_();
    //     });
    // }
    function updateDisplay() {
        // Try to initialise if this is run through a driven
        if (!initialized) {
            getRegRules();
        }

        // Stop execution on products without rules
        if (!conf.foundRules) {
            return;
        }

        const compRegs = rules[conf.selectedMainReg];
        log('Current selection: ', conf.selectedMainReg, compRegs);

        var hideAll = false;
        if (typeof compRegs === "undefined") {
            hideAll = true;
        }

        conf.allCompRegs.forEach(checkbox => {
            try {
                // Element to be hidden/shown
                const inputDiv = checkbox.closest(`div`);

                let found = false;

                if (!hideAll) {
                    for (let i = 0; i < compRegs.length; i++) {
                        const name = conf.input_compReg + "_" + compRegs[i];
                        if (name == checkbox.id) {
                            found = true;
                            break;
                        }
                    }
                }

                if (found) {
                    inputDiv.style.display = '';
                } else {
                    checkbox.checked = false;
                    inputDiv.style.display = 'none';
                }
            } catch (error) {
                // style.display isn't available straight away for some reason
                log('Regulation filter (waiting for element to be available):', error.message);
            }
        });

        // exceptionFilter(conf.selectedMainReg);
        validateComplementaryManually();
        setRegMatrix();
    }

    function setRegMatrix() {
        // TODO: move this function into this module
        setRange_DIM_TRAVEL_HEIGHT_H_range();

        const matrix = document.getElementById(conf.input_regMatrix);
        let strMatrix = [];

        // Get selected complementary regulations
        conf.selectedCompRegs.forEach(checkbox => {
            strMatrix.push(checkbox.name.replace(`${conf.input_compReg}_`, ''));
        });

        log('Regulation matrix', strMatrix);

        // Set input
        if (strMatrix.length < 1) {
            matrix.value = conf.matrix_no_match; // Set to no match
        } else {
            // The value is a comma separated list
            matrix.value = strMatrix.join(',');
        }

        // Trigger update
        matrix.dispatchEvent(new Event('change', { bubbles: true, cancelable: false }));
    }

    /**
     * Validate special rules for product regulations
     *
     *
     * New code structure
     * -- Product Region - i.e. _APAC and non _APAC
     *    -- Main Regulation
     *       -- Specific products - e.g. N_MINI, S_MONO
     *       -- Allowed combinations
     *       -- Other special rules
     * @param {*} input
     */
    function validateComplementary(input) {
        const comp = {
            //WHEN EN81-72 is selected and the user selects EN81-71 or EN81-73 then EN81-72 is to be deselected.
            EN81_71: document.getElementById(`${conf.input_compReg}_EN81_71`),
            EN81_72: document.getElementById(`${conf.input_compReg}_EN81_72`),
            EN81_73: document.getElementById(`${conf.input_compReg}_EN81_73`),
            EN81_77: document.getElementById(`${conf.input_compReg}_EN81_77`),

            //WHEN GOST_52382 is selected GOST_33653 is to be deselected.
            GOST_52382: document.getElementById(`${conf.input_compReg}_GOST_52382`),
            GOST_33652: document.getElementById(`${conf.input_compReg}_GOST_33652`),
            GOST_33653: document.getElementById(`${conf.input_compReg}_GOST_33653`),
            GOST_34305: document.getElementById(`${conf.input_compReg}_GOST_34305_2017`),

            // EN81-70 and LAW-13 are not allowed together.
            EN81_70: document.getElementById(`${conf.input_compReg}_EN81_70`),
            LAW_13: document.getElementById(`${conf.input_compReg}_LAW_13`)
        };

        if (!conf.product.includes('APAC')) {
            if (input == comp.EN81_71 || input == comp.EN81_73) {
                if (comp.EN81_71.checked || comp.EN81_73.checked) {
                    comp.EN81_72.checked = false;
                }
            }

            if (input == comp.EN81_72) {
                if (comp.EN81_72.checked) {
                    comp.EN81_71.checked = false;
                    comp.EN81_73.checked = false;
                }
            }

            if (input == comp.GOST_52382) {
                if (comp.GOST_52382.checked) {
                    comp.GOST_33653.checked = false;
                    comp.GOST_34305.checked = false;
                }
            }

            if (input == comp.GOST_33653) {
                if (comp.GOST_33653.checked) {
                    comp.GOST_52382.checked = false;
                    comp.GOST_34305.checked = false;
                }
            }

            if (input == comp.GOST_34305) {
                if (comp.GOST_34305.checked) {
                    comp.GOST_52382.checked = false;
                    comp.GOST_33653.checked = false;
                }
            }

            // EN81-70 and LAW-13 are not allowed together
            if (input == comp.EN81_70) {
                if (comp.LAW_13.checked) {
                    comp.LAW_13.checked = false;
                }
            }

            // Reverse: EN81-70 and LAW-13 are not allowed together
            if (input == comp.LAW_13) {
                if (comp.EN81_70.checked) {
                    comp.EN81_70.checked = false;
                }
            }
        } else if (conf.product.includes('TRANSYS')) {
            // EN81-73 and LAW-13 are not allowed together
            if (input == comp.EN81_73) {
                if (comp.LAW_13.checked) {
                    comp.LAW_13.checked = false;
                }
            }

            // Reverse: EN81-73 and LAW-13 are not allowed together
            if (input == comp.LAW_13) {
                if (comp.EN81_73.checked) {
                    comp.EN81_73.checked = false;
                }
            }
        } else if (conf.product.includes('APAC')) { // Asian SOC
            // Allowed combinations
            const combinations = {
                // Could be made main regulation specific
                EN81_70_72: [comp.EN81_70, comp.EN81_72],
                EN81_70_73: [comp.EN81_70, comp.EN81_73],
                GOST_52382_33652: [comp.GOST_52382, comp.GOST_33652],
            };

            // If none of the combinations is found, then only allow 1 input to be ticked
            let allowedCombination = [input]; // Default option
            // Default ignore list. Elements added to it won't be automatically enabled/disabled.
            const ignoreList = [];

            // Travel values. Parse int by bitshifting (prevents NaN)
            const travel = document.getElementById('DIM_TRAVEL_HEIGHT_H_ROUNDED').value | 0;

            // Fetch the current product release
            const release = parseFloat(document.getElementById('TYP_PRODUCT_RELEASE').value);

            const is = {
                nMini: conf.product.includes('N_MINI'),
                iMini: conf.product.includes('I_MINI'),
                nMono: conf.product.includes('N_MONO'),
                sMono: conf.product.includes('S_MONO'),
                eMono: conf.product.includes('E_MONO'),
                iMono: conf.product.includes('I_MONO'),
                tMini: conf.product.includes('3000_MINISPACE')
            };

            // Main reg combinations
            if (conf.selectedMainReg.includes('EN81')) {
                if (conf.hasBackdoor) { // TTC - Through type car

                    // If asian product and has backdoor, then 72 isn't allowed
                    if (release !== 21.1 || is.tMini) {
                        lockCheckbox(comp.EN81_72);
                        ignoreList.push(comp.EN81_72);
                    }

                    // If asian product and has backdoor, then 77 isn't allowed
                    lockCheckbox(comp.EN81_77);
                    ignoreList.push(comp.EN81_77);

                    // EN81-70
                    if (
                        is.nMono && travel > 90000 ||
                        is.sMono && travel > 120000 ||
                        is.eMono && travel > 75000 ||
                        is.nMini && travel > 160000
                    ) {
                        lockCheckbox(comp.EN81_70);
                        ignoreList.push(comp.EN81_70);
                    }

                    // EN81-72
                    if (
                        is.nMono && travel > 110000 && release === 21.1 ||
                        is.sMono && travel > 90000 && release === 21.1 ||
                        is.nMini && travel > 110000 && release === 21.1
                    ) {
                        lockCheckbox(comp.EN81_72);
                        ignoreList.push(comp.EN81_72);
                    }
                } else {

                    // EN81-70
                    if (
                        (is.nMono || is.sMono) && travel > 120000 ||
                        is.eMono && travel > 75000 ||
                        is.nMini && travel > 160000
                    ) {
                        lockCheckbox(comp.EN81_70);
                        ignoreList.push(comp.EN81_70);
                    }

                    // EN81-72
                    if (
                        is.nMono && travel > 110000 ||
                        is.sMono && travel > 90000 && release !== 20.1 ||
                        is.sMono && travel > 110000 && release === 20.1 ||
                        is.nMini && travel > 135000
                    ) {
                        lockCheckbox(comp.EN81_72);
                        ignoreList.push(comp.EN81_72);
                    }
                }

                // Disable EN81-77 if Max Travel is greater than these values:
                if (
                    is.nMono && travel > 75000 ||
                    is.eMono && travel > 75000 ||
                    is.sMono && travel > 75000 ||
                    is.nMini && travel > 75000 ||
                    is.tMini && travel > 100000
                ) {
                    lockCheckbox(comp.EN81_77);
                    ignoreList.push(comp.EN81_77);
                }

                // Asian EN combination or EN 72 especial rule:
                if (combinations.EN81_70_72.includes(input) && (is.nMono || is.sMono || is.nMini) && release >= 21.2) {
                    allowedCombination = combinations.EN81_70_72;
                } else if (combinations.EN81_70_73.includes(input)) {
                    allowedCombination = combinations.EN81_70_73;
                }

                blockSelection(allowedCombination, input, ignoreList);
            } else if (conf.selectedMainReg.includes('GOST')) {
                // Asian GOST combination

                // Same rule for SEC and TTC (with and without backdoor)
                if (
                    is.nMono && travel > 90000 ||
                    is.sMono && travel > 120000 ||
                    is.nMini && travel > 160000
                ) {
                    lockCheckbox(comp.GOST_52382);
                    ignoreList.push(comp.GOST_52382);
                }

                if (conf.hasBackdoor) {
                    if (is.nMono || is.eMono || is.nMini) {
                        lockCheckbox(comp.GOST_33652);
                        ignoreList.push(comp.GOST_33652);
                    }

                    // GOST-52382
                    if (is.sMono && travel > 90000) {
                        lockCheckbox(comp.GOST_33652);
                        ignoreList.push(comp.GOST_33652);
                    }
                } else {
                    // Determine maximum travel for combination
                    let combinedMax = 999999;
                    let gost33652Max = 999999;

                    if (is.nMono) {
                        combinedMax = 90000;
                        gost33652Max = 120000;
                    } else if (is.sMono) {
                        combinedMax = 120000;
                        gost33652Max = 120000;
                    } else if (is.nMini) {
                        combinedMax = 160000;
                        gost33652Max = 160000;
                    }

                    // - All asian products
                    // - Cannot have backdoor
                    // - Must not exceed maximum travel for combination
                    // - Only combination allowed
                    if (
                        combinations.GOST_52382_33652.includes(input) &&
                        comp.GOST_33652.checked &&
                        comp.GOST_52382.checked &&
                        travel <= combinedMax
                    ) {
                        allowedCombination = combinations.GOST_52382_33652;
                    } else {
                        if (travel > gost33652Max) {
                            lockCheckbox(comp.GOST_33652);
                            ignoreList.push(comp.GOST_33652);
                        }
                    }
                }

                blockSelection(allowedCombination, input, ignoreList);
            }
        }
    }

    // Change checkbox status - not to be used inside forEach loops.
    // inside validateComplementary (doesn't obey conf.disableCombinations).
    function lockCheckbox(checkbox, lock = true) {
        checkbox.disabled = lock;

        if (lock) {
            checkbox.checked = false;
        }

        log(`${lock ? 'Locked' : 'Unlocked'}`, checkbox);
    }

    // Run through all complementary regulations
    function validateComplementaryManually() {
        // We only need to run this through one of the visible options.
        const visible = Regulations.conf.visibleCompReg;
        const ticked = Regulations.conf.selectedCompRegs;

        if (ticked.length > 0) {
            // If any are ticked, running the event once on it will trigger the corect rules.
            // validateComplementary(ticked[0]);
            ticked.forEach(checkbox => validateComplementary(checkbox));

        } else if (visible.length > 0) {
            // Failing the above, run the rule on the first visible box
            validateComplementary(visible[0]);
        } else {
            console.warn('There are no visible complementary regulations.');
        }
    }

    // Change check box status
    // Exceptions are skipped
    function blockSelection(allowed = [], currentInput, ignored = []) {
        const regs = conf.visibleCompReg;
        let anyComboIsTicked = false;

        // Check if any combination element has been checked
        allowed.forEach(checkbox => {
            if (checkbox.checked) {
                anyComboIsTicked = true;
                return;
            }
        });

        // Run through all regulations
        regs.forEach(checkbox => {

            // If in ignore list, don't run
            if (!ignored.includes(checkbox)) {

                // If element is not allowed together with combination, uncheck and disable it
                if (!allowed.includes(checkbox) && anyComboIsTicked) {
                    // console.log('disabling');
                    checkbox.checked = false;
                    checkbox.disabled = conf.disableCombinations;
                } else {
                    // console.log('enabling');
                    checkbox.disabled = false;
                }
            }
        });
    }

    function init() {
        // Add attribute to prevent C1 from enabling/disabling inputs. Needed for custom rules.
        // See enableElements/disableElements override in desingUI.html
        conf.allCompRegs.forEach(checkbox => checkbox.setAttribute('skip-enabling', ''));

        // Initalise everything
        updateDisplay();

        try {
            // Regulations group header
            const group = document.querySelector(conf.regulationsGroupSelector);

            const validateOnce = () => {
                // Update complementary regulations. Doesn't work on load because the values aren't there yet.
                validateComplementaryManually();

                // Remove after one execution
                group.removeEventListener('click', validateOnce);
            };

            // Only run this event once
            group.addEventListener('click', validateOnce);
        } catch (error) {
            console.warn('Something went wrong.', error.message)
        }
    }

    return {
        get initialized() { return initialized; },
        get rules() { return rules; },
        conf: conf,

        // Methods
        init: init,
        getRegRules: getRegRules,
        updateDisplay: updateDisplay,
        setRegMatrix: setRegMatrix,
        validateComplementary: validateComplementary,
        validateComplementaryManually: validateComplementaryManually,
        // exceptionFilter: exceptionFilter,
    };
})();


// Run on load
$(() => {
    // If the original function is no longer defined, use this module.
    //Use the original function since that's already attached to input.
    if (typeof compRegFilter === 'undefined') {
        // Regulations.conf.debug = true;
        window.compRegFilter = Regulations.updateDisplay;
        Regulations.init();
    }
});
