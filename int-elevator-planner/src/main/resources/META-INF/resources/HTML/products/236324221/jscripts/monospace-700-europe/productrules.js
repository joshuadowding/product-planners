//MonoSpace 700
$(function () {
    rulesInit();
});

function rulesInit() {
    minSHCalc();
    minPHCalc();
    compRegFilter();
}


//MS700 minimum SH calculation
// Updated 080518 rejsli according to KTOC R17.1 changes LCE, ceiling height 70
// Checked against KTOC R18.2 and EN81-72 2015 rule (ceiling 150 mm) added rejsli 2019-05-19
// Updated for CH > 2400 cases  rejsli 2019-08-12
// Low headroom 2020-01-29 rejsli
function minSHCalc() {
    //console.debug("minSHCalc()");
    //get interface values
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var dim_car_height_temp = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val());
    var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
    var DIM_CAR_BALUSTRADE_HK = parseFloat($('[name=DIM_CAR_BALUSTRADE_HK]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var compRegEN81_72 = false;
    var DEFAULT_HEADROOM_HEIGHT = parseFloat($('[name=DEFAULT_HEADROOM_HEIGHT]').val());
    var HEADROOM_HEIGHT = parseFloat($('[name=HEADROOM_HEIGHT]').val());
    var DIM_HEADROOM_HEIGHT_MIN = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MIN]').val());
    var DIM_HEADROOM_HEIGHT_MAX = parseFloat($('[name=DIM_HEADROOM_HEIGHT_MAX]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var NO_HEADROOM_OPTION = $('[name=NO_HEADROOM_OPTION]').val();

    NO_HEADROOM_OPTION = 0;

    //check if EN81-72 selected
    if ($('#TYP_ELEV_STANDARD_COMPLEMENT_EN81_72').is(":checked")) {
        compRegEN81_72 = true;
    }

    if (DIM_CAR_BALUSTRADE_HK == 700) {
        if (VAL_RATED_SPEED <= 1.0) {
            if (WGT_RATED_LOAD_Q <= 1275) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    if (compRegEN81_72 == true) {
                        dim_car_height_temp += 1480;
                    } else {
                        dim_car_height_temp += 1400;
                    }
                } else {
                    if (compRegEN81_72 == true) {
                        dim_car_height_temp += 1530;
                    } else {
                        dim_car_height_temp += 1450;
                    }
                }
            }
            if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 2000 && compRegEN81_72 == false) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp = 3850;
                } else {
                    dim_car_height_temp += 1500;
                }
            }
            if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 1600 && compRegEN81_72 == true) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp = 3850;
                } else {
                    dim_car_height_temp += 1530;
                }
            }
            if (WGT_RATED_LOAD_Q > 1600 && WGT_RATED_LOAD_Q <= 2000 && compRegEN81_72 == true) {
                if (DIM_CAR_HEIGHT_CH < 2400) {
                    dim_car_height_temp = 3850;
                } else {
                    dim_car_height_temp += 1550;
                }
            }
            if (WGT_RATED_LOAD_Q > 2000) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp = 4050;
                } else {
                    dim_car_height_temp += 1700;
                }
            }
        }
        if (VAL_RATED_SPEED == 1.6) {
            if (WGT_RATED_LOAD_Q <= 1275) {
                if (compRegEN81_72 == true) {
                    if (DIM_CAR_HEIGHT_CH <= 2400) {
                        dim_car_height_temp += 1650;
                    } else {
                        dim_car_height_temp += 1700;
                    }
                } else {
                    if (DIM_CAR_HEIGHT_CH <= 2400) {
                        dim_car_height_temp += 1600;
                    } else {
                        dim_car_height_temp += 1650;
                    }
                }
            }
            if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 2000 && DIM_CAR_HEIGHT_CH <= 2200) {
                dim_car_height_temp = 3850;
            }
            if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 2000 && DIM_CAR_HEIGHT_CH > 2200 && DIM_CAR_HEIGHT_CH <= 2400) {
                if (compRegEN81_72 == true) {
                    dim_car_height_temp += 1650;
                } else {
                    dim_car_height_temp += 1600;
                }
            }
            if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 2000 && DIM_CAR_HEIGHT_CH > 2400) {
                if (compRegEN81_72 == true) {
                    dim_car_height_temp += 1700;
                } else {
                    dim_car_height_temp += 1650;
                }
            }
            if (WGT_RATED_LOAD_Q > 2000 && compRegEN81_72 == false) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp = 4050;
                } else {
                    dim_car_height_temp += 1700;
                }
            }
            if (WGT_RATED_LOAD_Q > 2000 && compRegEN81_72 == true) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp = 4075;
                } else {
                    dim_car_height_temp += 1725;
                }
            }
        }
        if (VAL_RATED_SPEED == 1.75 || VAL_RATED_SPEED == 2.0) {
            if (WGT_RATED_LOAD_Q <= 1150) {
                if (DIM_CAR_HEIGHT_CH < 2300) {
                    dim_car_height_temp = 4050;
                } else {
                    if (compRegEN81_72 == false) {
                        if (DIM_CAR_HEIGHT_CH <= 2400) {
                            dim_car_height_temp += 1800;
                        } else {
                            dim_car_height_temp += 1850;
                        }
                    } else {
                        if (DIM_CAR_HEIGHT_CH <= 2400) {
                            dim_car_height_temp += 1850;
                        } else {
                            dim_car_height_temp += 1900;
                        }
                    }
                }
            }
            if (WGT_RATED_LOAD_Q > 1150 && WGT_RATED_LOAD_Q <= 2000) {
                if (DIM_CAR_HEIGHT_CH <= 2200) {
                    dim_car_height_temp = 4200;
                } else {
                    if (DIM_CAR_HEIGHT_CH <= 2400) {
                        dim_car_height_temp += 2000;
                    } else {
                        dim_car_height_temp += 2050;
                    }
                }
            }
        }
        if (VAL_RATED_SPEED == 2.5) {
            if (WGT_RATED_LOAD_Q <= 1000) {
                if (DIM_CAR_HEIGHT_CH < 2300) {
                    dim_car_height_temp = 4400;
                } else {
                    if (DIM_CAR_HEIGHT_CH == 2400) {
                        if (compRegEN81_72 == false) {
                            dim_car_height_temp = 4500;
                        } else {
                            dim_car_height_temp = 4550;
                        }
                    } else {
                        if (compRegEN81_72 == false) {
                            dim_car_height_temp += 2150;
                        } else {
                            dim_car_height_temp += 2200;
                        }
                    }
                }
            }
            if (WGT_RATED_LOAD_Q > 1000 && WGT_RATED_LOAD_Q <= 1600) {
                if (DIM_CAR_HEIGHT_CH < 2400) {
                    dim_car_height_temp = 4500;
                } else {
                    if (DIM_CAR_HEIGHT_CH == 2400) {
                        if (compRegEN81_72 == false) {
                            dim_car_height_temp = 4500;
                        } else {
                            dim_car_height_temp = 4550;
                        }
                    } else {
                        if (compRegEN81_72 == false) {
                            dim_car_height_temp += 2150;
                        } else {
                            dim_car_height_temp += 2200;
                        }
                    }
                }
            }
            if (WGT_RATED_LOAD_Q > 1600) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp = 4650;
                } else {
                    dim_car_height_temp += 2300;
                }
            }
        }
    } else {
        if (VAL_RATED_SPEED <= 1.0) {
            if (DIM_CAR_HEIGHT_CH <= 2400) {
                if (WGT_RATED_LOAD_Q <= 1275 && compRegEN81_72 == false) {
                    dim_car_height_temp += 1800;
                }
                if (WGT_RATED_LOAD_Q <= 1275 && compRegEN81_72 == true) {
                    dim_car_height_temp += 1850;
                }
                if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 2000) {
                    dim_car_height_temp += 1850;
                }
                if (WGT_RATED_LOAD_Q > 2000) {
                    if (DIM_CAR_HEIGHT_CH < 2300) {
                        dim_car_height_temp = 4050;
                    } else {
                        dim_car_height_temp += 1850;
                    }
                }
            } else {
                if (WGT_RATED_LOAD_Q <= 1275 && compRegEN81_72 == false) {
                    dim_car_height_temp += 1850;
                }
                if (WGT_RATED_LOAD_Q <= 1275 && compRegEN81_72 == true) {
                    dim_car_height_temp += 1900;
                }
                if (WGT_RATED_LOAD_Q > 1275) {
                    dim_car_height_temp += 1900;
                }
            }
        }
        if (VAL_RATED_SPEED == 1.6) {
            if (DIM_CAR_HEIGHT_CH <= 2400) {
                if (WGT_RATED_LOAD_Q <= 2000) {
                    dim_car_height_temp += 2000;
                }
                if (WGT_RATED_LOAD_Q > 2000) {
                    if (DIM_CAR_HEIGHT_CH < 2400) {
                        dim_car_height_temp += 2200;
                    } else {
                        dim_car_height_temp = 4400;
                    }
                }
            } else {
                dim_car_height_temp += 2050;
            }
        }
        if (VAL_RATED_SPEED == 1.75 || VAL_RATED_SPEED == 2.0) {
            if (WGT_RATED_LOAD_Q <= 1150 && compRegEN81_72 == false) {
                if (DIM_CAR_HEIGHT_CH < 2400) {
                    dim_car_height_temp += 2200;
                } else {
                    dim_car_height_temp += 2250;
                }
            }
            if (WGT_RATED_LOAD_Q <= 1150 && compRegEN81_72 == true) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp += 2250;
                } else {
                    dim_car_height_temp += 2300;
                }
            }
            if (WGT_RATED_LOAD_Q > 1150 && WGT_RATED_LOAD_Q <= 2000) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp += 2300;
                } else {
                    dim_car_height_temp += 2350;
                }
            }
        }
        if (VAL_RATED_SPEED == 2.5) {
            if (WGT_RATED_LOAD_Q <= 1150 && compRegEN81_72 == false) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp += 2500;
                } else {
                    dim_car_height_temp += 2550;
                }
            }
            if (WGT_RATED_LOAD_Q <= 1150 && compRegEN81_72 == true) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp += 2550;
                } else {
                    dim_car_height_temp += 2600;
                }
            }
            if (WGT_RATED_LOAD_Q > 1150 && WGT_RATED_LOAD_Q <= 1600) {
                if (DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp += 2500;
                } else {
                    dim_car_height_temp += 2550;
                }
            }
            if (WGT_RATED_LOAD_Q > 1600) {
                if (DIM_CAR_HEIGHT_CH == 2100) {
                    dim_car_height_temp = 4650;
                }
                if (DIM_CAR_HEIGHT_CH > 2100 && DIM_CAR_HEIGHT_CH <= 2400) {
                    dim_car_height_temp += 2500;
                }
                if (DIM_CAR_HEIGHT_CH > 2400) {
                    dim_car_height_temp += 2550;
                }
            }
        }
    }
 

    //set interface input
    DIM_HEADROOM_SH.val(dim_car_height_temp);
    
    // Only in NL the extension is visible in SH, 2019-08-21 rejsli
    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();
    var Extension = 0

    if (Loc == 'NL' || Loc == 'NL192') { //Netherlands
        var Extension = 200;
    }
  
    var headroom = parseFloat(DIM_HEADROOM_SH.val()) + Extension;
    DIM_HEADROOM_SH = headroom;
    
    document.ui["DIM_HEADROOM_SH"].value = DIM_HEADROOM_SH;
  
    //Low headroom
    //Default and max value to Headroom, 2020-01-29 rejsli
    //If HEADROOM_AND_PIT_SECTION = 1, visible
    //KPT-1736 1600 and 1350 fixed 2020-12-07 rejsli
    if (HEADROOM_AND_PIT_SECTION == 1) {
        var DIM_CAR_HEIGHT_CH = parseFloat($('input[name=DIM_CAR_HEIGHT_CH]:checked').val())
        var warning = 0;
	

    	   if (DIM_CAR_BALUSTRADE_HK == 700) {
      	if (WGT_RATED_LOAD_Q >= 630 && WGT_RATED_LOAD_Q < 1275) {
            DIM_HEADROOM_HEIGHT_MIN = 3100;
		}
      	if (WGT_RATED_LOAD_Q == 1275){
            DIM_HEADROOM_HEIGHT_MIN = 3200;
		}
	   }else{
          if (WGT_RATED_LOAD_Q >= 630 && WGT_RATED_LOAD_Q < 1275) {
		   if  (DIM_CAR_HEIGHT_CH == 2100) {
			  DIM_HEADROOM_HEIGHT_MIN = 3150;
		   }else{
			  DIM_HEADROOM_HEIGHT_MIN= 3250;
		   }
		 }  
	      if (WGT_RATED_LOAD_Q==1275){
 		 	if  (DIM_CAR_HEIGHT_CH == 2100) {
		     	DIM_HEADROOM_HEIGHT_MIN = 3200;
		 	}else{
		 		DIM_HEADROOM_HEIGHT_MIN= 3250;
			}
		 }
	   }
	   if (WGT_RATED_LOAD_Q >= 1350 && WGT_RATED_LOAD_Q <= 1600) {
	    	 DIM_HEADROOM_HEIGHT_MIN= 3325;
	   }
	   if (WGT_RATED_LOAD_Q >= 1800 && WGT_RATED_LOAD_Q <= 2000) {
	    	 DIM_HEADROOM_HEIGHT_MIN= 3475;
	   }	 
 
        DEFAULT_HEADROOM_HEIGHT = DIM_HEADROOM_SH;

	   if (DIM_CAR_BALUSTRADE_HK = 700) {
		  if (VAL_RATED_SPEED == 1.0) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
		    }
		  if (VAL_RATED_SPEED == 1.6) {
			  DIM_HEADROOM_HEIGHT_MAX = 4500;
		  }
		  if (VAL_RATED_SPEED == 2.0 || VAL_RATED_SPEED == 1.75) {
		    if (WGT_RATED_LOAD_Q  >= 630 && WGT_RATED_LOAD_Q  <= 1150) {
			 if (DIM_CAR_HEIGHT_CH < 2700) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			 }
			 if (DIM_CAR_HEIGHT_CH == 2700) {
			   DIM_HEADROOM_HEIGHT_MAX = 4550;
			 }
		    }
		    if (WGT_RATED_LOAD_Q  >= 630 && WGT_RATED_LOAD_Q <= 1150) {
			 if (DIM_CAR_HEIGHT_CH < 2700)  {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			   }
			 if (DIM_CAR_HEIGHT_CH == 2700) {
			   DIM_HEADROOM_HEIGHT_MAX = 4550;
			 }
		    }
		    if (WGT_RATED_LOAD_Q>= 1275 && WGT_RATED_LOAD_Q <= 2000) {
			 if (DIM_CAR_HEIGHT_CH >= 2500) {
			   DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2050;
			 }
			 if (DIM_CAR_HEIGHT_CH < 2500) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			 }
		    }
		  }
		  if (VAL_RATED_SPEED == 2.5) {
			 DIM_HEADROOM_HEIGHT_MAX = 5000;
		  }
	   }else{
		  if (VAL_RATED_SPEED == 1.0) {
		    if (WGT_RATED_LOAD_Q  >= 630 && WGT_RATED_LOAD_Q <= 1275) {
			   if (DIM_CAR_HEIGHT_CH < 2700) {
				DIM_HEADROOM_HEIGHT_MAX = 4500;
				 }
			   if (DIM_CAR_HEIGHT_CH == 2700) {
				DIM_HEADROOM_HEIGHT_MAX = 4550;
			   }
		    }
		    if (WGT_RATED_LOAD_Q  >= 1350 && WGT_RATED_LOAD_Q <= 2500) {
			 if (DIM_CAR_HEIGHT_CH < 2700) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			 }
			 if (DIM_CAR_HEIGHT_CH == 2700) {
			   DIM_HEADROOM_HEIGHT_MAX = 4600;
			 }
		    }
		  }
		  if (VAL_RATED_SPEED == 1.6) {
		    if (WGT_RATED_LOAD_Q >= 630 && WGT_RATED_LOAD_Q <= 2000) {
			 if (DIM_CAR_HEIGHT_CH < 2500) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			 }else{
			   DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2050;
			 }
		    }
		    if (WGT_RATED_LOAD_Q >= 2275 && WGT_RATED_LOAD_Q <= 2500) {
			 if (DIM_CAR_HEIGHT_CH < 2400) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			 }
			 if (DIM_CAR_HEIGHT_CH == 2400) {
			   DIM_HEADROOM_HEIGHT_MAX = 4600;
			 }
			 if (DIM_CAR_HEIGHT_CH > 2400) {
			   DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2050;
			 }
		    }
		  }
		  if (VAL_RATED_SPEED == 2.0 || VAL_RATED_SPEED == 1.75) {
		    if (WGT_RATED_LOAD_Q >= 630 && WGT_RATED_LOAD_Q <= 1150) {
			   if (DIM_CAR_HEIGHT_CH < 2400) {
				DIM_HEADROOM_HEIGHT_MAX = 4500;
			   }
			   if (DIM_CAR_HEIGHT_CH == 2400) {
				DIM_HEADROOM_HEIGHT_MAX = 4600;
			   }
			   if (DIM_CAR_HEIGHT_CH > 2400) {
				DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2250;
			   }
		    }
		    if (WGT_RATED_LOAD_Q >= 1275 && WGT_RATED_LOAD_Q <= 2000) {
			 if (DIM_CAR_HEIGHT_CH <= 2200) {
			   DIM_HEADROOM_HEIGHT_MAX = 4500;
			 }
			 if (DIM_CAR_HEIGHT_CH > 2200 && DIM_CAR_HEIGHT_CH < 2500) {
			   DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2300;
			 }
			 if (DIM_CAR_HEIGHT_CH >= 2500) {
			   DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2350;
			 }
		    }
		  }
		  if (VAL_RATED_SPEED == 2.5){
		    if (WGT_RATED_LOAD_Q  >= 630 && WGT_RATED_LOAD_Q <= 1000) {
			   if (DIM_CAR_HEIGHT_CH < 2500) {
				DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2500;
			   }else{
				DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2550;
			   }
		    }
		    if (WGT_RATED_LOAD_Q >= 1150 && WGT_RATED_LOAD_Q  <= 1600) {
			 if (DIM_CAR_HEIGHT_CH <= 2400) {
			   DIM_HEADROOM_HEIGHT_MAX = 5000;
			 }
			 if (DIM_CAR_HEIGHT_CH > 2400) {
			   DIM_HEADROOM_HEIGHT_MAX = DIM_CAR_HEIGHT_CH + 2550;
			 }
		    }
		  }
		  
	   }

        if (TYP_REGULATION_EN8121 == 1) {
            warning = DEFAULT_HEADROOM_HEIGHT;
        } else {
            DIM_HEADROOM_HEIGHT_MIN = headroom;
		  warning = headroom;
        }

        if (isNaN(HEADROOM_HEIGHT) || HEADROOM_HEIGHT < DIM_HEADROOM_HEIGHT_MIN || HEADROOM_HEIGHT > DIM_HEADROOM_HEIGHT_MAX) {
            HEADROOM_HEIGHT = DIM_HEADROOM_SH;
        } else {
            HEADROOM_HEIGHT = HEADROOM_HEIGHT;
        }

        //set interface inputs
        document.ui["DIM_HEADROOM_HEIGHT_MIN"].value = DIM_HEADROOM_HEIGHT_MIN;
        document.ui["DIM_HEADROOM_HEIGHT_MAX"].value = DIM_HEADROOM_HEIGHT_MAX;
        document.ui["DEFAULT_HEADROOM_HEIGHT"].value = DEFAULT_HEADROOM_HEIGHT;
        document.ui["HEADROOM_HEIGHT"].value = HEADROOM_HEIGHT;
        document.ui["DIM_HEADROOM_SH"].value = HEADROOM_HEIGHT;
        document.ui["HEADROOM_HEIGHT_low"].value = DIM_HEADROOM_HEIGHT_MIN;
        document.ui["HEADROOM_HEIGHT_high"].value = DIM_HEADROOM_HEIGHT_MAX;
	 
  	   document.getElementById('HEADROOM_HEIGHT').setAttribute('warninglow', warning);
	   updateDataTableInput(document.ui.HEADROOM_HEIGHT);
	 
    }

    //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
    forceChangeEvent(document.ui.DIM_HEADROOM_SH);
    
    if(document.ui.NO_HEADROOM_OPTION)
        forceChangeEvent(document.ui.NO_HEADROOM_OPTION);
}

//M700 Pit depth 2020-01-29 rejsli
function minPHCalc() {

    var DIM_PIT_HEIGHT_PH = parseFloat($('[name=DIM_PIT_HEIGHT_PH]').val());
    var DEFAULT_PIT_DEPTH = parseFloat($('[name=DEFAULT_PIT_DEPTH]').val());
    var TEMP_MIN_PIT = parseFloat($('[name=TEMP_MIN_PIT]').val());
    var PIT_DEPTH = parseFloat($('[name=PIT_DEPTH]').val());
    var DIM_PIT_DEPTH_MIN = parseFloat($('[name=PIT_DEPTH_MIN]').val());
    var DIM_PIT_DEPTH_MAX = parseFloat($('[name=PIT_DEPTH_MAX]').val());
    var TYP_REGULATION_EN8121 = parseFloat($('[name=TYP_REGULATION_EN8121]').val());
    var WGT_RATED_LOAD_Q = parseFloat($('[name=WGT_RATED_LOAD_Q_EU]').val());
    var VAL_RATED_SPEED = parseFloat($('[name=VAL_RATED_SPEED_EU]').val());
    var DIM_TRAVEL_HEIGHT_H = parseFloat($('[name=DIM_TRAVEL_HEIGHT_H]').val());
    var HEADROOM_AND_PIT_SECTION = parseFloat($('[name=HEADROOM_AND_PIT_SECTION]').val());
    var Warning = 0;
    var Normal = 0;
  

    if (HEADROOM_AND_PIT_SECTION == 1) {
	 
	   //Normal pit depth EN81-20 
        if (VAL_RATED_SPEED == 1.0) {
		if (WGT_RATED_LOAD_Q <= 1150){
		  Normal = 1200;
		}
		if (WGT_RATED_LOAD_Q == 1275) { 
		   Normal = 1250;
		}	 
		if (WGT_RATED_LOAD_Q > 1275 && WGT_RATED_LOAD_Q <= 1600) { 
		   Normal = 1300;
		}
		if (WGT_RATED_LOAD_Q > 1600 && WGT_RATED_LOAD_Q <= 2275 ) { 
		   Normal = 1400;
		}
		if (WGT_RATED_LOAD_Q = 2500) { 
		    Normal = 1425;
		}
	   }
        if (VAL_RATED_SPEED == 1.6) {
		if (WGT_RATED_LOAD_Q < 1275){
		  Normal = 1350;
		}
		if (WGT_RATED_LOAD_Q >= 1275 && WGT_RATED_LOAD_Q <= 1600) { 
		   Normal = 1400;
		}	 
		if (WGT_RATED_LOAD_Q > 1600 && WGT_RATED_LOAD_Q <= 2275) { 
		   Normal = 1500;
		}
		if (WGT_RATED_LOAD_Q > 2275) { 
		   Normal = 1550;
		}
	   }
        if (VAL_RATED_SPEED == 1.75 || VAL_RATED_SPEED == 2.0) {
		if (WGT_RATED_LOAD_Q <= 1000){
		  Normal = 1675;
		}	 
		if (WGT_RATED_LOAD_Q >= 1150 && WGT_RATED_LOAD_Q <= 1600) { 
		   Normal = 1700;
		}
		if (WGT_RATED_LOAD_Q > 1600) { 
		   Normal = 1850;
		}
	   }
        if (VAL_RATED_SPEED == 2.5) {
		if (WGT_RATED_LOAD_Q <= 1000){
		  Normal = 1950;
		}	 
		if (WGT_RATED_LOAD_Q > 1000) { 
		   Normal = 2250;
		}
	   }
	 
	   if (VAL_RATED_SPEED == 1.0) {
		if (WGT_RATED_LOAD_Q <= 1275){
		  DIM_PIT_DEPTH_MAX = 1750;
		}
		if (WGT_RATED_LOAD_Q > 1275) { 
		   DIM_PIT_DEPTH_MAX = 2000;
		}	 
	   }
	   if (VAL_RATED_SPEED == 1.6) {
		if (WGT_RATED_LOAD_Q <= 1600){
		  DIM_PIT_DEPTH_MAX = 2000;
		}
		if (WGT_RATED_LOAD_Q > 1600) { 
		   DIM_PIT_DEPTH_MAX = 2200;
		}	 
	   } 
	   if (VAL_RATED_SPEED > 1.6 ) {	
		   DIM_PIT_DEPTH_MAX = 2500;
	   } 
	 
	 
        if (TYP_REGULATION_EN8121 == 1) {
            DIM_PIT_DEPTH_MIN = TEMP_MIN_PIT;
        } else {
            DIM_PIT_DEPTH_MIN = Normal;
        }

        DEFAULT_PIT_DEPTH = Normal;
	   Warning = Normal;

        if (isNaN(PIT_DEPTH) || PIT_DEPTH < DIM_PIT_DEPTH_MIN || PIT_DEPTH > DIM_PIT_DEPTH_MAX) {
            PIT_DEPTH = Normal;
        } else {
            PIT_DEPTH = PIT_DEPTH;
        }

	   DIM_PIT_HEIGHT_PH = PIT_DEPTH;
	 
        //set interface inputs
        document.ui["DIM_PIT_DEPTH_MIN"].value = DIM_PIT_DEPTH_MIN;
        document.ui["DIM_PIT_DEPTH_MAX"].value = DIM_PIT_DEPTH_MAX;
        document.ui["DEFAULT_PIT_DEPTH"].value = DEFAULT_PIT_DEPTH;
        document.ui["PIT_DEPTH"].value = PIT_DEPTH;
        document.ui["DIM_PIT_HEIGHT_PH"].value = DIM_PIT_HEIGHT_PH;
        //set range
        document.ui["PIT_DEPTH_low"].value = DIM_PIT_DEPTH_MIN;
        document.ui["PIT_DEPTH_high"].value = DIM_PIT_DEPTH_MAX;

	   document.getElementById('PIT_DEPTH').setAttribute('warninglow', Warning);  
	   updateDataTableInput(document.ui.PIT_DEPTH);
	
    	   //this always needs to be run last so the changes can take effect
        forceChangeEvent(document.ui.DIM_PIT_HEIGHT_PH);
    }
}

function UserRoleOther() {
    //get interface values

    var USER_ROLE = document.ui.USER_ROLE.value;

    if (USER_ROLE == "OTHER") {
        $('#OTHER_USER_ROLE_TR_tag').show();
    }

    else {
        $('#OTHER_USER_ROLE_TR_tag').hide();
    }
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
//2019-02-20 rejsli release 18.2 GOST_33984.1-2016 added
//2019-10-01 rejsli KPT-383 LAW 13
//2019-10-03 C1-VC	Changed format to be location dependant and have faster access.
function compRegFilter_backup() {
    //var product = $('#SELECT_ELEVATOR option:selected').val();
    var product = "MONOSPACE_700_EUR"; //hard code this as is only a reference to what the product is
    var mainReg = $('input:radio[name=TYP_ELEV_STANDARD]:checked').val();

    var inputName = "ANON_USE_GRP";
    var Loc = $('#' + inputName).val();

    var regulations = [];

    // Country overrides
    switch (Loc) {
	   case 'BA': // Bosnia
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'BG': // Bulgaria
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'CY': // Cyprus
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'EE':  // Estonia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;
	   
	   case 'ES': // Spain
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'GR': // Greece
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'HR': // Croatia
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'IE':  // Ireland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
	   	  break;
	   
        case 'IS':  // Iceland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
            break;

        case 'IT':  // Italy
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77', 'LAW_13'];
            break;
	   
	   case'LT': //Lithuania
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
	       break;
	   
	   case'LV': //Latvia
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
	       break;
	   
	   case 'ME': // Montenegro
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'MK':  // Macedonia
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
		  break;
	   
	   case 'PT': // Portugal
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;

        case 'PL':  // Poland
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;
	   
	   case 'RO': // Romania
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case 'RS': // Serbia
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;
	   
	   case'SI': //Slovenia
	   	  regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73'];
	   	  break;
	   
	   case 'TR': // Turkey
	       regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
 	       regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
	       break;

        case 'UK':  // United Kingdom
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            break;

        default:    // Defaults values if location has no match
            regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['EN81-20'] = ['None', 'EN81_70', 'EN81_71', 'EN81_72', 'EN81_73', 'EN81_77'];
            regulations['GOST_53780'] = ['GOST_52382', 'GOST_33652', 'GOST_33653'];
            regulations['GOST_33984.1-2016'] = ['GOST_52382', 'GOST_33652', 'GOST_33653'];
            regulations['Pubel_2012'] = ['None'];
            break;
    }

    var getReg = regulations[mainReg];

    applyCompRegFilter(getReg);
}


