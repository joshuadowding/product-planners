// N MonoSpace (Asia)
$(function () {
  rulesInit();
});

function rulesInit() {
  minSHCalc();
  compRegFilter();
}

function minPHCalc() {
  // Doesn't do anything. Prevents breaking filters as this function was attached
  // directly to inputs.
  return;
}

// NMonoSpace minimum SH calculation acc to DL1-02.08.001 M.2
// KET = Taiwan not in code yet -> Elevator planner is not going to be implemented
// no need for separate rules
// GOST_33984.1-2016 added, rejsli 100419
// SH_CONST not defined default value 1480 set rejsli 231019
function minSHCalc() {
  // get interface values
  var VAL_RATED_SPEED = parseFloat($("[name=VAL_RATED_SPEED_EA]").val());
  var DIM_CAR_HEIGHT_CH = parseFloat($("input[name=DIM_CAR_HEIGHT_CH]:checked").val());
  var DIM_HEADROOM_SH = $("[name=DIM_HEADROOM_SH]");
  var DIM_CAR_BALUSTRADE_HK = parseFloat($("[name=DIM_CAR_BALUSTRADE_HK]").val());
  var DIM_TRAVEL_HEIGHT_H = parseFloat($("[name=DIM_TRAVEL_HEIGHT_H]").val());
  var MainReg = $("input:radio[name=TYP_ELEV_STANDARD]:checked").val();
  // var SH_CONST = $('[name=SH_CONST]').val();
  var SH_CONST = parseFloat($("[name=SH_CONST] option:selected").text().trim());

  if (!isNaN(SH_CONST)) {
    // 		if (SH_CONST.length < 10) {
    // 			if (SH_CONST.length > 7) {
    // 				SH_CONST = SH_CONST.substring(7, SH_CONST.length);
    // 			} else {
    // 				SH_CONST = SH_CONST.substring(6, SH_CONST.length);
    // 			}
    // 		} else {
    // 			if (SH_CONST.length > 10) {
    // 				SH_CONST = SH_CONST.substring(7, SH_CONST.length);
    // 			} else {
    // 				SH_CONST = SH_CONST.substring(6, SH_CONST.length);
    // 			}
    // 		}

    var Addition = parseFloat(SH_CONST);
  } else {
    var Addition = parseFloat(1480);
  }

  // First values with HK = 700 are caluculated then if HK is higher
  // the additions are done in the end

  if (MainReg == "MS2021") {
    if (Addition == 0) {
      if (VAL_RATED_SPEED == 1.0) {
        DIM_CAR_HEIGHT_CH = 3650;
      } else if (VAL_RATED_SPEED == 1.6) {
        DIM_CAR_HEIGHT_CH = 3820;
      } else if (VAL_RATED_SPEED == 1.75) {
        DIM_CAR_HEIGHT_CH = 3940;
      }
    } else {
      DIM_CAR_HEIGHT_CH += Addition;
    }
  }

  if (MainReg == "EN81-1" || MainReg == "EN81-20" || MainReg == "GB7588" || MainReg == "AS1735.1_EN81-1" || MainReg == "AS1735.1_EN81-20") {
    if (Addition == 0 || VAL_RATED_SPEED >= 2.0) {
      if (VAL_RATED_SPEED == 1.0) {
        DIM_CAR_HEIGHT_CH = 3580;
      } else if (VAL_RATED_SPEED == 1.6) {
        DIM_CAR_HEIGHT_CH = 3770;
      } else if (VAL_RATED_SPEED == 1.75) {
        DIM_CAR_HEIGHT_CH = 3790;
      } else if (VAL_RATED_SPEED == 2.0) {
        if (DIM_TRAVEL_HEIGHT_H > 90000 && DIM_CAR_HEIGHT_CH <= 2400) {
          DIM_CAR_HEIGHT_CH = 4700;
        } else {
          DIM_CAR_HEIGHT_CH += Addition;
        }
      } else if (VAL_RATED_SPEED == 2.5) {
        if (DIM_TRAVEL_HEIGHT_H > 90000 && DIM_CAR_HEIGHT_CH <= 2400) {
          DIM_CAR_HEIGHT_CH = 4900;
        } else {
          DIM_CAR_HEIGHT_CH += Addition;
        }
      }
    } else {
      DIM_CAR_HEIGHT_CH += Addition;
    }
  }

  if (MainReg == "Pubel_2003" || MainReg == "GOST_53780" || MainReg == "GOST_33984.1-2016" || MainReg == "Pubel_2012") {
    if (Addition == 0 || VAL_RATED_SPEED >= 2.0) {
      if (VAL_RATED_SPEED == 1.0) {
        DIM_CAR_HEIGHT_CH = 3580;
      } else if (VAL_RATED_SPEED == 1.6) {
        DIM_CAR_HEIGHT_CH = 3770;
      } else if (VAL_RATED_SPEED == 1.75) {
        DIM_CAR_HEIGHT_CH = 3790;
      } else if (VAL_RATED_SPEED == 2.0) {
        if (DIM_TRAVEL_HEIGHT_H > 90000 && DIM_CAR_HEIGHT_CH <= 2400) {
          DIM_CAR_HEIGHT_CH = 4300;
        } else {
          DIM_CAR_HEIGHT_CH += Addition;
        }
      } else if (VAL_RATED_SPEED == 2.5) {
        if (DIM_TRAVEL_HEIGHT_H > 90000 && DIM_CAR_HEIGHT_CH <= 2400) {
          DIM_CAR_HEIGHT_CH = 4500;
        } else {
          DIM_CAR_HEIGHT_CH += Addition;
        }
      }
    } else {
      DIM_CAR_HEIGHT_CH += Addition;
    }
  }

  if (MainReg == "SS550") {
    if (Addition == 0 || VAL_RATED_SPEED >= 2.0) {
      if (VAL_RATED_SPEED == 1.0) {
        DIM_CAR_HEIGHT_CH = 3810;
      } else if (VAL_RATED_SPEED == 1.6) {
        DIM_CAR_HEIGHT_CH = 3960;
      } else if (VAL_RATED_SPEED == 1.75) {
        DIM_CAR_HEIGHT_CH = 3990;
      } else if (VAL_RATED_SPEED == 2.0) {
        if (DIM_TRAVEL_HEIGHT_H > 90000 && DIM_CAR_HEIGHT_CH <= 2400) {
          DIM_CAR_HEIGHT_CH = 4300;
        } else {
          DIM_CAR_HEIGHT_CH += Addition;
        }
      } else if (VAL_RATED_SPEED == 2.5) {
        if (DIM_TRAVEL_HEIGHT_H > 90000 && DIM_CAR_HEIGHT_CH <= 2400) {
          DIM_CAR_HEIGHT_CH = 4500;
        } else {
          DIM_CAR_HEIGHT_CH += Addition;
        }
      }
    } else {
      DIM_CAR_HEIGHT_CH += Addition;
    }
  }

  if (DIM_CAR_BALUSTRADE_HK == 900) {
    DIM_CAR_HEIGHT_CH += 200;
  } else if (DIM_CAR_BALUSTRADE_HK == 1100) {
    DIM_CAR_HEIGHT_CH += 400;
  }

  //set interface input
  DIM_HEADROOM_SH.val(DIM_CAR_HEIGHT_CH);

  //logging
  //console.info("WGT_RATED_LOAD_Q=" + WGT_RATED_LOAD_Q);
  //console.info("VAL_RATED_SPEED=" + VAL_RATED_SPEED);
  //console.info("DIM_CAR_HEIGHT_CH=" + DIM_CAR_HEIGHT_CH);
  //console.info("DIM_HEADROOM_SH=" + DIM_HEADROOM_SH.val());
  //console.info("DIM_CAR_BALUSTRADE_HK=" + DIM_CAR_BALUSTRADE_HK);
  //console.info("SH_CONST=" + SH_CONST);

  //get Country Headroom Extension value from attribute, then add to DIM_HEADROOM_SH
  // Not needed for Asion products
  /*var inputName = "KONE_COUNTRY_C";
     var inputValue = $('#' + inputName).val();
     var attributeName = "Headroom_Extension";
     var query = "?id=" + configurationId
     + "&iName=" + inputName
     + "&iVal=" + inputValue
     + "&aName=" + attributeName;
     $.getJSON("/spr/Configuration/interface/api/beta/attribute" + query, function (data) {
     //	parse data
     try {
     if (data.option[0]) {
     var attrValue = parseFloat(data.option[0].result[0].value[0]);//attribute value
     var DIM_HEADROOM_SH = $('[name=DIM_HEADROOM_SH]');
     var headroom = parseFloat(DIM_HEADROOM_SH.val()) + attrValue;
     DIM_HEADROOM_SH.val(headroom);

     //logging
     console.info("Attribute Value=" + attrValue);
     console.info("DIM_HEADROOM_SH=" + DIM_HEADROOM_SH.val());

     setValues();//update values for tables
     }
     } catch (err) {
     dumpError(err);
     }
     });
     */

  //this always needs to be run last so the DIM_HEADROOM_SH chnages can take effect
  forceChangeEvent(document.ui.DIM_HEADROOM_SH);
}

// ------------------------------------------------------------------------------------------------
// Regulation setup
// ------------------------------------------------------------------------------------------------
/*
 * update Complementary Regulation checkbox list to show options related to Main Regulation and Product
 */
// GOST_33984.1-2016 added, rejsli 100419
//2019-10-03 C1-VC	Changed format to be location dependant and faster
//2020-05-15 C1-VC	Disabled to test new regulations method.
// 				Rename to 'compRegFilter' to revert to this method.
function compRegFilter_backup() {
  //var product = $('#SELECT_ELEVATOR option:selected').val();
  var product = "N_MONOSPACE_APAC"; //hard code this as is only a reference to what the product is

  var mainReg = $("input:radio[name=TYP_ELEV_STANDARD]:checked").val();

  var inputName = "ANON_USE_GRP";
  var Loc = $("#" + inputName).val();

  var regulations = [];

  // Country overrides
  switch (Loc) {
    // Switch statement example
    // case 'UK':
    // regulations['E_MONOSPACE_APAC EN81-1'] = ['None', 'EN81_70'];
    // break;
    case "BG": // Bulgaria
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "CY": // Cyprus
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    /*case 'SG': //Singapore - removed as EN-81-20 has to be main regulation currently
	   	  regulations['EN81-1'] = ['None', 'EN81_70', 'EN81_72', 'EN81_73', 'EN81_77'];
	   	  break;*/

    case "ZA": // South Africa
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "UG": // Uganda
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "MA": // Morocco
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "AE": // UAE
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "KE": // Kenya
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "GR": // Greece
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "ME": // Montenegro
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "MK": // Macedonia
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "RO": // Romania
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "RS": // Serbia
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "SEMA":
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "SA": //Saudi Arabia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "BH": //Bahrain
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "QA": //Qatar
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "OM": //Oman
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "SI": //Slovenia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73"];
      break;

    case "TR": //Turkey, EN81-71 removed and EWN81-73 from EN81-1 acc to product data localization 2020-04-21 rejsli
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_72"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_72", "EN81_73"];
      break;

    case "TH": //Thailand
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "PH": //Philippines
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "VN": //Vietnam
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "ID": //Indonesia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "MY": // Malaysia
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_71", "EN81_72", "EN81_73", "EN81_77"];
      break;

    case "RU": // Russia
      regulations["GOST_53780"] = ["None", "GOST_52382", "GOST_33652"];
      regulations["GOST_33984.1-2016"] = ["None", "GOST_52382", "GOST_33652"];
      break;

    default:
      regulations["EN81-1"] = ["None", "EN81_70", "EN81_72"];
      regulations["EN81-20"] = ["None", "EN81_70", "EN81_72", "EN81_73"];
      regulations["GOST_53780"] = ["None", "GOST_52382", "GOST_33652"];
      regulations["GOST_33984.1-2016"] = ["None", "GOST_52382", "GOST_33652"];
      //regulations['N_MONOSPACE_APAC GB7588'] = ['None', 'GB26465'];
      regulations["Pubel_2012"] = ["None"];
      regulations["Pubel_2003"] = ["None"];
      regulations["SS550"] = ["None"];
      regulations["MS2021"] = ["None"];
      regulations["AS1735.1_EN81-1"] = ["None", "None", "None"];
      regulations["AS1735.1_EN81-20"] = ["None", "None", "None", "None"];
      break;
  }

  var getReg = regulations[mainReg];
  applyCompRegFilter(getReg);
}

/*
 * function to set travel height range for input DIM_TRAVEL_HEIGHT_H
 */
function DIM_TRAVEL_HEIGHT_H_Range(params) {
  var DIM_TRAVEL_HEIGHT_H_Range_Total_travel = {
    //TYP_ELEV_STANDARD
    "EN81-1": {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    "EN81-20": {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    GB7588: {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    GOST_53780: {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    "GOST_33984.1-2016": {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    Pubel_2003: {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    Pubel_2012: {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 120000 },
    },
    MS2021: {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 90000 },
      2: { low: 2550, high: 90000 },
    },
    SS550: {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 120000 },
      2: { low: 2550, high: 90000 },
    },
    "AS1735.1_EN81-1": {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 75000 },
      2: { low: 2550, high: 75000 },
    },
    "AS1735.1_EN81-20": {
      //QTY_CAR_ENTRANCES
      1: { low: 2550, high: 75000 },
      2: { low: 2550, high: 75000 },
    },
    "SS550_2020": {
      1: { low: 2550, high: 75000 },
      2: { low: 2550, high: 75000 },
    }
  };

  var QTY_CAR_ENTRANCES = document.ui.QTY_CAR_ENTRANCES.value;

  if (params) {
    QTY_CAR_ENTRANCES = "1";
    if (params.checkFront && params.checkBack) {
      QTY_CAR_ENTRANCES = "2";
    }
  }

  var standard = DIM_TRAVEL_HEIGHT_H_Range_Total_travel[document.ui.TYP_ELEV_STANDARD.value][QTY_CAR_ENTRANCES];

  let releaseSelector = $("#TYP_PRODUCT_RELEASE");
  let countrySelector = $("#ANON_USE_GRP");

  if (countrySelector !== null && releaseSelector !== null) {
    let currentCountry = countrySelector.val();
    let currentRelease = releaseSelector.val();

    if (currentCountry === "NZ" && standard.high > 75000) {
      standard.high = 75000; // Add an exception for New Zealand so that the standard.high caps at 75,000 (KPT-1969):
    }

    if (currentCountry === "SG" && currentRelease === "21.2") {
      let option = document.querySelector("input[value*='SS550:2019']");
      if (option !== null) {
        if (option.checked) {
          let radio = option.parentElement.querySelector("input[type=radio]");
          if (radio !== null) radio.checked = true;
        }

        $(option.parentElement).show();
      }

      option = document.querySelector("input[value*='SS550:2020']");
      if (option !== null) {
        $(option.parentElement).show();

        if (!option.checked) {
          let radio = option.parentElement.querySelector("input[type=radio]");
          if (radio !== null) radio.checked = false;
        }
      }
    } else if (currentCountry === "SG" && currentRelease !== "21.2") {
      let option = document.querySelector("input[value*='SS550:2020']");
      if (option !== null) {
        if (option.checked) {
          let radio = option.parentElement.querySelector("input[type=radio]");
          if (radio !== null) radio.checked = false;
        }

        $(option.parentElement).hide();
      }

      option = document.querySelector("input[value*='SS550:2019']");
      if (option !== null) {
        $(option.parentElement).show();

        if (!option.checked) {
          let radio = option.parentElement.querySelector("input[type=radio]");
          if (radio !== null) radio.checked = true;
        }
      }
    }
  }

  setGroupHeaderValues(null, true);

  document.ui["DIM_TRAVEL_HEIGHT_H_low"].value = standard.low;
  document.ui["DIM_TRAVEL_HEIGHT_H_high"].value = standard.high;
  document.ui["DIM_TRAVEL_HEIGHT_H_warninglow"].value = standard.low;
  document.ui["DIM_TRAVEL_HEIGHT_H_warninghigh"].value = standard.high;
}
