// products\247109161\jscripts\selectorrules.js

/*
 *  Hide or Show occupants details based on CATEGORY.
 */

$(() => {
    OccupantsHide();
    PassengerGoodsHide();
});

function OccupantsHide() {
    //console.debug("OccupantsHide");

    // Get interface values:
    var POPULATION_PASSENGER_GOODS = $('input:radio[name=POPULATION_PASSENGER_GOODS]:checked').val();
    if (POPULATION_PASSENGER_GOODS == undefined) {
        $('#SEPARATOR_1_TR_tag').hide();
        $('#ELEVATOR_LANDINGS_HEADER_HTML_TR_tag').hide();
        $('#QTY_SERVED_LDO_SLIDER_TR_tag').hide();

        $('#SEPARATOR_2_TR_tag').hide();
        $('#GET_RECOMMENDATION_BUTTON_HTML_TR_tag').hide();
        $('#QTY_PASSENGERS_TR_tag').hide();
        $('#NUMBER_PASSANGERS_HTML_TR_tag').hide();
        $('#LOAD_TR_tag').hide();
        $('#ELEVATOR_LOAD_HTML_TR_tag').hide();
        $('#QTY_OCCUPANTS_SLIDER_TR_tag').hide();
        //$('#QTY_OCCUPANTS_TR_tag').hide();
        $('#NUMBER_OF_OCCUPANTS_HEADER_HTML_TR_tag').hide();
    } else {
        if (POPULATION_PASSENGER_GOODS == "PASSENGER") {
            $('#SEPARATOR_1_TR_tag').show();
            $('#ELEVATOR_LANDINGS_HEADER_HTML_TR_tag').show();
            $('#QTY_SERVED_LDO_SLIDER_TR_tag').show();

            $('#QTY_SERVED_LDO_SLIDERslider').slider('option', 'disabled', false);
            $('#QTY_SERVED_LDO_SLIDERslider').slider('enable');

            $('#SEPARATOR_2_TR_tag').show();
            $('#GET_RECOMMENDATION_BUTTON_HTML_TR_tag').show();
            $('#QTY_PASSENGERS_TR_tag').show();
            $('#NUMBER_PASSANGERS_HTML_TR_tag').show();
            $('#LOAD_TR_tag').hide();
            $('#ELEVATOR_LOAD_HTML_TR_tag').hide();
            $('#QTY_OCCUPANTS_SLIDER_TR_tag').hide();
            //$('#QTY_OCCUPANTS_TR_tag').hide();
            $('#NUMBER_OF_OCCUPANTS_HEADER_HTML_TR_tag').hide();

            $('#QTY_PASSENGERSslider').slider('option', 'disabled', false);
            $('#QTY_PASSENGERSslider').slider('enable');
        } else if (POPULATION_PASSENGER_GOODS == "GOODS") {
            $('#SEPARATOR_1_TR_tag').show();
            $('#ELEVATOR_LANDINGS_HEADER_HTML_TR_tag').show();
            $('#QTY_SERVED_LDO_SLIDER_TR_tag').show();

            $('#QTY_SERVED_LDO_SLIDERslider').slider('option', 'disabled', false);
            $('#QTY_SERVED_LDO_SLIDERslider').slider('enable');

            $('#SEPARATOR_2_TR_tag').show();
            $('#GET_RECOMMENDATION_BUTTON_HTML_TR_tag').show();
            $('#QTY_PASSENGERS_TR_tag').hide();
            $('#NUMBER_PASSANGERS_HTML_TR_tag').hide();
            $('#LOAD_TR_tag').show();
            $('#ELEVATOR_LOAD_HTML_TR_tag').show();
            $('#QTY_OCCUPANTS_SLIDER_TR_tag').hide();
            //$('#QTY_OCCUPANTS_TR_tag').hide();
            $('#NUMBER_OF_OCCUPANTS_HEADER_HTML_TR_tag').hide();

            $('#LOADslider').slider('option', 'disabled', false);
            $('#LOADslider').slider('enable');
        } else if (POPULATION_PASSENGER_GOODS == "POPULATION") {
            $('#SEPARATOR_1_TR_tag').show();
            $('#ELEVATOR_LANDINGS_HEADER_HTML_TR_tag').show();
            $('#QTY_SERVED_LDO_SLIDER_TR_tag').show();

            $('#QTY_SERVED_LDO_SLIDERslider').slider('option', 'disabled', false);
            $('#QTY_SERVED_LDO_SLIDERslider').slider('enable');

            $('#SEPARATOR_2_TR_tag').show();
            $('#GET_RECOMMENDATION_BUTTON_HTML_TR_tag').show();
            $('#QTY_PASSENGERS_TR_tag').hide();
            $('#NUMBER_PASSANGERS_HTML_TR_tag').hide();
            $('#LOAD_TR_tag').hide();
            $('#ELEVATOR_LOAD_HTML_TR_tag').hide();
            $('#QTY_OCCUPANTS_SLIDER_TR_tag').show();
            //$('#QTY_OCCUPANTS_TR_tag').show();
            $('#NUMBER_OF_OCCUPANTS_HEADER_HTML_TR_tag').show();

            //$('#QTY_OCCUPANTS_SLIDERslider').slider('option', 'disabled', false);
            //$('#QTY_OCCUPANTS_SLIDERslider').slider('enable');
        } else if (POPULATION_PASSENGER_GOODS == "POPULATION_GOODS") {
            $('#SEPARATOR_1_TR_tag').show();
            $('#ELEVATOR_LANDINGS_HEADER_HTML_TR_tag').show();
            $('#QTY_SERVED_LDO_SLIDER_TR_tag').show();

            $('#QTY_SERVED_LDO_SLIDERslider').slider('option', 'disabled', false);
            $('#QTY_SERVED_LDO_SLIDERslider').slider('enable');

            $('#SEPARATOR_2_TR_tag').show();
            $('#GET_RECOMMENDATION_BUTTON_HTML_TR_tag').show();
            $('#QTY_PASSENGERS_TR_tag').hide();
            $('#NUMBER_PASSANGERS_HTML_TR_tag').hide();
            $('#LOAD_TR_tag').show();
            $('#ELEVATOR_LOAD_HTML_TR_tag').show();
            $('#QTY_OCCUPANTS_SLIDER_TR_tag').show();
            //$('#QTY_OCCUPANTS_TR_tag').show();
            $('#NUMBER_OF_OCCUPANTS_HEADER_HTML_TR_tag').show();

            $('#LOADslider').slider('option', 'disabled', false);
            $('#LOADslider').slider('enable');
        }
    }
}

function PassengerGoodsHide() {
    //console.debug("PassengerGoodsHide");

    // Get interface values:
    var TYP_ELEVATOR_RANGE_OF_USE = $('#SELECTOR_TYP_ELEVATOR_RANGE_OF_USE option:selected').val();

    if (TYP_ELEVATOR_RANGE_OF_USE == "PS") {
        $('#PASSENGER_GOODS_HTML_TR_tag').hide();
        $('#POPULATION_PASSENGER_GOODS_TR_tag').hide();
        $('#BUILDING_CATEGORY_HTML_TR_tag').hide();
        $('#CATEGORY_TR_tag').hide();
    } else {
        if (TYP_ELEVATOR_RANGE_OF_USE == "OFFICE" || TYP_ELEVATOR_RANGE_OF_USE == "HOTEL" || TYP_ELEVATOR_RANGE_OF_USE == "RETAIL") {
            $('#PASSENGER_GOODS_HTML_TR_tag').show();
            $('#POPULATION_PASSENGER_GOODS_TR_tag').show();
        } else {
            $('#PASSENGER_GOODS_HTML_TR_tag').hide();
            $('#POPULATION_PASSENGER_GOODS_TR_tag').hide();
        }
    
        if (TYP_ELEVATOR_RANGE_OF_USE == "LEISURE_EDUCATION" || TYP_ELEVATOR_RANGE_OF_USE == "INDUSTRIAL") {
            $('#BUILDING_CATEGORY_HTML_TR_tag').hide();
            $('#CATEGORY_TR_tag').hide();
        } else {
            $('#BUILDING_CATEGORY_HTML_TR_tag').show();
            $('#CATEGORY_TR_tag').show();
        }
    }
}
